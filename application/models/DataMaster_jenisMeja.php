<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataMaster_jenisMeja extends CI_Model {

	public function list_all() {
		$q=$this->db->select('jk.*')
					->from('tb_jenis_meja as jk')
					->get();
		return $q->result();
	}
	public function upload(){
		$config['upload_path'] = './assets/photoMeja/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']	= '2048';
		$config['remove_space'] = TRUE;
		
		$new_name = mt_rand(1000,9999).($_FILES['file_gambar']['name']);
		$config['file_name'] = $new_name;

		$this->load->library('upload', $config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file_gambar')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}
	
	// Fungsi untuk menyimpan data ke database
	public function save($upload){
		$data = array(
			'ukuran'=>$this->input->post('ukuran'),
			'nama'=>$this->input->post('nama'),
			'foto_meja' => $upload['file']['file_name'],
		);
		//var_dump($data);
		$this->db->insert('tb_jenis_meja', $data);
		$this->session->set_flashdata('msg_alert', 'Data Jenis Meja berhasil ditambahkan');
	}
	public function update($id,$status,$upload)
	{
		if ($status == 1) {
			$data = array(
				'ukuran'=>$this->input->post('ukuran'),
				'nama'=>$this->input->post('nama'),
				'foto_meja' => $upload['file']['file_name'],
			);
		}
		else{
			$data = array(
				'ukuran'=>$this->input->post('ukuran'),
				'nama'=>$this->input->post('nama'),
				'foto_meja' => $this->input->post('old'),
			);
		}
		//var_dump($data);
		$this->db->where('id_jenis_meja',$id)
			 ->update('tb_jenis_meja', $data);
		$this->session->set_flashdata('msg_alert', 'Data Kursi berhasil diupdate');
	}
	public function hapusGambar($old)
	{
		return array_map('unlink', glob(FCPATH."assets/photoMeja/$old"));
	}
	public function hapusJenisMeja($id)
	{
		$old = $this->db->select('foto_meja')
						->from('tb_jenis_meja')
						->where('id_jenis_meja',$id)
						->get();
		$old = $old->row()->foto_meja;
		//var_dump($old);
		$this->hapusGambar($old);
		$this->db->where('id_jenis_meja',$id)
			 ->delete('tb_jenis_meja');
  		$this->session->set_flashdata('msg_alert', 'Data Jenis Meja berhasil dihapus');
	}
}