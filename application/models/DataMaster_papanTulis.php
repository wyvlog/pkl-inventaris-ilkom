<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataMaster_papanTulis extends CI_Model {

	public function list_all() {
		$q=$this->db->select('p.*, ji.nama_barang as nama_barang, r.nama as ruangan, gd.nama as gedung, ji.type, ji.tahun, ji.kode_barang')
					->from('tb_papan as p')
					->join('tb_ruangan as r','p.id_ruangan = r.id_ruangan')
					->join('tb_gedung as gd','r.id_gedung = gd.id_gedung')
					->join('tb_jenis_inventaris as ji','p.id_kode_barang = ji.id_kode_barang')
					->get();
		return $q->result();
	}
	public function gedung()
	{
		$data = $this->db->select('*')
						 ->from('tb_gedung')
						 ->get();
		//var_dump($data);
		return $data->result();
	}
	public function tb_jenis_inventaris()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_inventaris')
						 ->where('nama_barang','Papan')
						 ->get();
		return $d->result();
	}

	public function ruangan($postData){
    $response = array();

    // Select record
    $this->db->select('*');
    $this->db->where('id_gedung', $postData['gedung']);
    $q = $this->db->get('tb_ruangan');
    $response = $q->result_array();

    return $response;
  }
  public function tambahPapan($data)
  {
	$this->db->insert('tb_papan', $data);
	$this->session->set_flashdata('msg_alert', 'Data Papan berhasil ditambahkan');
  }
  public function hapusPapan($id)
  {
  	//var_dump($id);
  	$this->db->where('id_papan',$id)
			 ->delete('tb_papan');
  	$this->session->set_flashdata('msg_alert', 'Data Papan berhasil dihapus');

  }
  public function editPapan($id)
  {
  	$data = $this->db->select('p.*, ji.nama_barang, r.nama as ruangan, gd.nama as gedung, gd.id_gedung as id_gedung, ji.type, ji.tahun')
  			 ->from('tb_papan as p')
			 ->join('tb_ruangan as r','p.id_ruangan = r.id_ruangan')
			 ->join('tb_gedung as gd','r.id_gedung = gd.id_gedung')
			 ->join('tb_jenis_inventaris as ji','p.id_kode_barang = ji.id_kode_barang')
  			 ->where('id_papan',$id)
  			 ->get();
  	//var_dump($data);
  	return $data->row();
  }
  public function updatePapan($id,$data)
  {
	$this->db->where('id_papan',$id)
			 ->update('tb_papan', $data);
	$this->session->set_flashdata('msg_alert', 'Data Papan berhasil diupdate');
  }

}
