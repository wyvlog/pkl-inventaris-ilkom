<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Model {

	public function list_all() {
		$q=$this->db->select('a.*, ji.nama_barang as nama_barang, r.nama as ruangan, gd.nama as gedung, ji.type, ji.tahun, ji.kode_barang')
					->from('tb_ac as a')
					->join('tb_ruangan as r','a.id_ruangan = r.id_ruangan')
					->join('tb_gedung as gd','r.id_gedung = gd.id_gedung')
					->join('tb_jenis_inventaris as ji','a.id_kode_barang = ji.id_kode_barang')
					->get();
		return $q->result();
	}
	public function kursi($table)
	{
		$data = $this->db->select('*')
						 ->from($table)
						 ->get();
		return $data->result();
	}
	public function tahun()
	{
		$tahun = $this->db->select('tahun, COUNT(tahun) as total')
			->from('tb_jenis_inventaris as ji')
			->join('tb_kursi as k','ji.id_kode_barang = k.id_kode_barang')
			->group_by('tahun')
			->order_by('tahun', 'asc')
			->get();


		return $tahun->result();
	}

}
