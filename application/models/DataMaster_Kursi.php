<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataMaster_Kursi extends CI_Model {

	public function list_all() {
		$q=$this->db->select('k.*, jk.nama as jenisKursi, r.nama as ruangan, gd.nama as gedung, ji.type, ji.tahun, ji.kode_barang')
					->from('tb_kursi as k')
					->join('tb_jenis_kursi as jk','k.id_jenis_kursi = jk.id_jenis_kursi')
					->join('tb_ruangan as r','k.id_ruangan = r.id_ruangan')
					->join('tb_gedung as gd','r.id_gedung = gd.id_gedung')
					->join('tb_jenis_inventaris as ji','k.id_kode_barang = ji.id_kode_barang')
					->get();
		return $q->result();
	}
	public function gedung()
	{
		$data = $this->db->select('*')
						 ->from('tb_gedung')
						 ->get();
		//var_dump($data);
		return $data->result();
	}
	public function tb_jenis_inventaris()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_inventaris')
						 ->where('nama_barang','Kursi')
						 ->get();
		return $d->result();
	}
	public function jenis()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_kursi')
						 ->get();
		return $d->result();
	}

	public function ruangan($postData){
    $response = array();

    // Select record
    $this->db->select('*');
    $this->db->where('id_gedung', $postData['gedung']);
    $q = $this->db->get('tb_ruangan');
    $response = $q->result_array();

    return $response;
  }
  public function tambahKursi($data)
  {
	$this->db->insert('tb_kursi', $data);
	$this->session->set_flashdata('msg_alert', 'Data Kursi berhasil ditambahkan');
  }
  public function hapusKursi($id)
  {
  	//var_dump($id);
  	$this->db->where('id_kursi',$id)
			 ->delete('tb_kursi');
  	$this->session->set_flashdata('msg_alert', 'Data Kursi berhasil dihapus');

  }
  public function editKursi($id)
  {
  	$data = $this->db->select('k.*, jk.nama as jenisKursi,jk.id_jenis_kursi, r.nama as ruangan, gd.nama as gedung, gd.id_gedung as id_gedung, ji.type, ji.tahun')
  			 ->from('tb_kursi as k')
			 ->join('tb_jenis_kursi as jk','k.id_jenis_kursi = jk.id_jenis_kursi')
			 ->join('tb_ruangan as r','k.id_ruangan = r.id_ruangan')
			 ->join('tb_gedung as gd','r.id_gedung = gd.id_gedung')
			 ->join('tb_jenis_inventaris as ji','k.id_kode_barang = ji.id_kode_barang')
  			 ->where('id_kursi',$id)
  			 ->get();
  	//var_dump($data);
  	return $data->row();
  }
  public function updateKursi($id,$data)
  {
	$this->db->where('id_kursi',$id)
			 ->update('tb_kursi', $data);
	$this->session->set_flashdata('msg_alert', 'Data Kursi berhasil diupdate');
  }

}
