<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataMaster_PeminjamanProyektor extends CI_Model {

	public function list_all() {
		$q=$this->db->select('p.*,r.nama as namaRuangan')
					->from('tb_peminjaman_proyektor as p')
					->join('tb_ruangan as r','p.id_ruangan = r.id_ruangan')
					->order_by('id_peminjaman', 'desc')
					->get();
		//var_dump($q->result());
		return $q->result();
	}
	public function tb_proyektor()
	{
		$xx = $this->db->select('*')
						->from('tb_peminjaman_proyektor')
						->where('status',0)
						->get();
		if($xx->result() == null)
		{
			$y = 0;
		}
		else
		{
			$y = $xx->row()->id_proyektor;
		}
		//$y = 0;
		$d = $this->db->select('*')
						 ->from('tb_proyektor')
						 ->where_not_in('id_proyektor', $y)
						 ->get();
		//var_dump($xx->result());
		return $d->result();
	}
	public function gedung()
	{
		$d = $this->db->select('*')
						 ->from('tb_gedung')
						 ->get();
		return $d->result();
	}

  public function pinjamProyektor($data)
  {
  	//var_dump($data);
	$this->db->insert('tb_peminjaman_proyektor', $data);
	$this->session->set_flashdata('msg_alert', 'Data Proyektor berhasil ditambahkan');
  }
  public function hapusPeminjamanProyektor($id)
  {
  	//var_dump($id);
  	$this->db->where('id_peminjaman',$id)
			 ->delete('tb_peminjaman_proyektor');
  	$this->session->set_flashdata('msg_alert', 'Data Peminjaman Proyektor berhasil dihapus');

  }
  public function editPeminjamanProyektor($id,$data)
  {
  	$this->db->where('id_peminjaman', $id)
			  ->update('tb_peminjaman_proyektor', $data);
	$this->session->set_flashdata('msg_alert', 'Proyektor berhasil diedit');
  }
  public function kembaliProyektor($id,$data)
  {
  	//var_dump($data);
	$this->db->where('id_peminjaman', $id)
			  ->update('tb_peminjaman_proyektor', $data);
	$this->session->set_flashdata('msg_alert', 'Proyektor berhasil dikembalikan');
  }

}
