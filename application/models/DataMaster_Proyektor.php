<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataMaster_Proyektor extends CI_Model {

	public function list_all() {
		$q=$this->db->select('p.*, ji.nama_barang as nama_barang, ji.type, ji.tahun, ji.kode_barang,')
					->from('tb_proyektor as p')
					->join('tb_jenis_inventaris as ji','p.id_kode_barang = ji.id_kode_barang')
					->get();
		return $q->result();
	}
	public function tb_jenis_inventaris()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_inventaris')
						 ->where('nama_barang','Proyektor')
						 ->get();
		return $d->result();
	}

	public function type()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_inventaris')
						 ->where('kode_barang','2.12.01.02.001')
						 ->get();
		return $d->result();
	}

  public function tambahProyektor($data)
  {
  	//var_dump($data);
	$this->db->insert('tb_proyektor', $data);
	$this->session->set_flashdata('msg_alert', 'Data Proyektor berhasil ditambahkan');
  }
  public function hapusProyektor($id)
  {
  	//var_dump($id);
  	$this->db->where('id_proyektor',$id)
			 ->delete('tb_proyektor');
  	$this->session->set_flashdata('msg_alert', 'Data Proyektor berhasil dihapus');

  }
  public function editProyektor($id)
  {
  	$data = $this->db->select('p.*, ji.nama_barang, ji.type, ji.tahun')
  			 ->from('tb_proyektor as p')
			 ->join('tb_jenis_inventaris as ji','p.id_kode_barang = ji.id_kode_barang')
  			 ->where('id_proyektor',$id)
  			 ->get();
  	//var_dump($data);
  	return $data->row();
  }
  public function updateProyektor($id,$data)
  {
	$this->db->where('id_proyektor',$id)
			 ->update('tb_proyektor', $data);
	$this->session->set_flashdata('msg_alert', 'Data Proyektor berhasil diupdate');
  }

}
