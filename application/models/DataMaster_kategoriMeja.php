<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataMaster_kategoriMeja extends CI_Model {

	public function list_all() {
		$q=$this->db->select('*')
					->from('tb_jenis_meja')
					->get();
		return $q->result();
	}
  public function tambahKategoriMeja($data)
  {
	$this->db->insert('tb_jenis_meja', $data);
	$this->session->set_flashdata('msg_alert', 'Data Kategori berhasil ditambahkan');
  }
  public function hapusKategoriMeja($id)
  {
  	//var_dump($id);
  	$this->db->where('id_jenis_meja',$id)
			 ->delete('tb_jenis_meja');
  	$this->session->set_flashdata('msg_alert', 'Data Kategori Meja berhasil dihapus');

  }
  public function editKategoriMeja($id)
  {
  	$data = $this->db->select('*')
  			 ->from('tb_jenis_meja')
  			 ->where('id_jenis_meja',$id)
  			 ->get();
  	//var_dump($data);
  	return $data->row();
  }
  public function updateKategoriMeja($id,$data)
  {
	$this->db->where('id_jenis_meja',$id)
			 ->update('tb_jenis_meja', $data);
	$this->session->set_flashdata('msg_alert', 'Data Kategori Meja berhasil diupdate');
  }

}
