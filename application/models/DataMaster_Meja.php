<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataMaster_Meja extends CI_Model {

	public function list_all() {
		$q=$this->db->select('k.*, jk.nama as jenisMeja, r.nama as ruangan, gd.nama as gedung, ji.type as tipeMeja, ji.tahun as tahun')
					->from('tb_meja as k')
					->join('tb_jenis_meja as jk','k.id_jenis_meja = jk.id_jenis_meja')
					->join('tb_ruangan as r','k.id_ruangan = r.id_ruangan')
					->join('tb_gedung as gd','r.id_gedung = gd.id_gedung')
					->join('tb_jenis_inventaris as ji','k.id_kode_barang = ji.id_kode_barang')
					->get();
		return $q->result();
	}

	public function tb_jenis_inventaris()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_inventaris')
						 ->where('nama_barang','Meja')
						 ->get();
		return $d->result();
	}

	public function gedung()
	{
		$data = $this->db->select('*')
						 ->from('tb_gedung')
						 ->get();
		//var_dump($data);
		return $data->result();
	}
	public function type()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_inventaris')
						 ->where('kode_barang','023.04.22.415253.000.2011')
						 ->get();
		return $d->result();
	}
	public function jenis()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_meja')
						 ->get();
		return $d->result();
	}
	
	public function ruangan($postData){
    $response = array();
 
    // Select record
    $this->db->select('*');
    $this->db->where('id_gedung', $postData['gedung']);
    $q = $this->db->get('tb_ruangan');
    $response = $q->result_array();

    return $response;
  }
  public function tambahMeja($data)
  {
	$this->db->insert('tb_meja', $data);
	$this->session->set_flashdata('msg_alert', 'Data Meja berhasil ditambahkan');
  }
  public function hapusMeja($id)
  {
  	//var_dump($id);
  	$this->db->where('id_meja',$id)
			 ->delete('tb_meja');
  	$this->session->set_flashdata('msg_alert', 'Data Meja berhasil dihapus');

  }
  public function editMeja($id)
  {
  	$data = $this->db->select('k.*, jk.nama as jenisMeja,jk.id_jenis_meja, r.nama as ruangan, gd.nama as gedung, gd.id_gedung as id_gedung, ji.type, ji.tahun')
  			 ->from('tb_meja as k')
			 ->join('tb_jenis_meja as jk','k.id_jenis_meja = jk.id_jenis_meja')
			 ->join('tb_ruangan as r','k.id_ruangan = r.id_ruangan')
			 ->join('tb_gedung as gd','r.id_gedung = gd.id_gedung')
			 ->join('tb_jenis_inventaris as ji','k.id_kode_barang = ji.id_kode_barang')
  			 ->where('id_meja',$id)
  			 ->get();
  	//var_dump($data);
  	return $data->row();
  }
  public function updateMeja($id,$data)
  {
	$this->db->where('id_meja',$id)
			 ->update('tb_meja', $data);
	$this->session->set_flashdata('msg_alert', 'Data Meja berhasil diupdate');
  }

}