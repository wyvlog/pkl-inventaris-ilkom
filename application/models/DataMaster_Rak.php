<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataMaster_Rak extends CI_Model {

	public function list_all() {
		$q=$this->db->select('k.*, jk.nama as jenisRak, r.nama as ruangan, gd.nama as gedung, ji.type as tipeRak, ji.tahun as tahun')
					->from('tb_rak as k')
					->join('tb_jenis_rak as jk','k.id_jenis_rak = jk.id_jenis_rak')
					->join('tb_ruangan as r','k.id_ruangan = r.id_ruangan')
					->join('tb_gedung as gd','r.id_gedung = gd.id_gedung')
					->join('tb_jenis_inventaris as ji','k.id_kode_barang = ji.id_kode_barang')
					->get();
		return $q->result();
	}

	public function tb_jenis_inventaris()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_inventaris')
						 ->where('nama_barang','Rak')
						 ->get();
		return $d->result();
	}

	public function gedung()
	{
		$data = $this->db->select('*')
						 ->from('tb_gedung')
						 ->get();
		//var_dump($data);
		return $data->result();
	}
	public function type()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_inventaris')
						 ->where('kode_barang','3.04.01.04.004.164')
						 ->get();
		return $d->result();
	}
	public function jenis()
	{
		$d = $this->db->select('*')
						 ->from('tb_jenis_rak')
						 ->get();
		return $d->result();
	}
	
	public function ruangan($postData){
    $response = array();
 
    // Select record
    $this->db->select('*');
    $this->db->where('id_gedung', $postData['gedung']);
    $q = $this->db->get('tb_ruangan');
    $response = $q->result_array();

    return $response;
  }
  public function tambahRak($data)
  {
	$this->db->insert('tb_rak', $data);
	$this->session->set_flashdata('msg_alert', 'Data Rak berhasil ditambahkan');
  }
  public function hapusRak($id)
  {
  	//var_dump($id);
  	$this->db->where('id_rak',$id)
			 ->delete('tb_rak');
  	$this->session->set_flashdata('msg_alert', 'Data Rak berhasil dihapus');

  }
  public function editRak($id)
  {
  	$data = $this->db->select('k.*, jk.nama as jenisRak,jk.id_jenis_rak, r.nama as ruangan, gd.nama as gedung, gd.id_gedung as id_gedung, ji.type, ji.tahun')
  			 ->from('tb_rak as k')
			 ->join('tb_jenis_rak as jk','k.id_jenis_rak = jk.id_jenis_rak')
			 ->join('tb_ruangan as r','k.id_ruangan = r.id_ruangan')
			 ->join('tb_gedung as gd','r.id_gedung = gd.id_gedung')
			 ->join('tb_jenis_inventaris as ji','k.id_kode_barang = ji.id_kode_barang')
  			 ->where('id_rak',$id)
  			 ->get();
  	//var_dump($data);
  	return $data->row();
  }
  public function updateRak($id,$data)
  {
	$this->db->where('id_rak',$id)
			 ->update('tb_rak', $data);
	$this->session->set_flashdata('msg_alert', 'Data Rak berhasil diupdate');
  }

}