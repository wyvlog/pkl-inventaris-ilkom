<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataMaster_KatInven extends CI_Model {

	public function list_all() {
		$q=$this->db->select('*')
					->from('tb_jenis_inventaris')
					->get();
		return $q->result();
	}
  public function upload(){
    $config['upload_path'] = './assets/photoJenisKategori/';
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['max_size'] = '2048';
    $config['remove_space'] = TRUE;
    
    $new_name = mt_rand(1000,9999).($_FILES['file_gambar']['name']);
    $config['file_name'] = $new_name;

    $this->load->library('upload', $config); // Load konfigurasi uploadnya
    if($this->upload->do_upload('file_gambar')){ // Lakukan upload dan Cek jika proses upload berhasil
      // Jika berhasil :
      $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
      return $return;
    }else{
      // Jika gagal :
      $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
      return $return;
    }
  }
  
  // Fungsi untuk menyimpan data ke database
  public function save($upload){
    $data = array(
      'kode_barang'=>$this->input->post('kode_barang'),
      'id_univ'=>$this->input->post('id_univ'),
      'type'=>$this->input->post('type'),
      'nama_barang'=>$this->input->post('nama_barang'),
      'tahun'=>$this->input->post('tahun'),
      'foto' => $upload['file']['file_name'],
    );
    //var_dump($data);
    $this->db->insert('tb_jenis_inventaris', $data);
    $this->session->set_flashdata('msg_alert', 'Data Jenis Kategori berhasil ditambahkan');
  }
	
  public function update($id,$status,$upload)
  {
    if ($status == 1) {
      $data = array(
      'kode_barang'=>$this->input->post('kode_barang'),
      'id_univ'=>$this->input->post('id_univ'),
      'type'=>$this->input->post('type'),
      'nama_barang'=>$this->input->post('nama_barang'),
      'tahun'=>$this->input->post('tahun'),
      'foto' => $upload['file']['file_name'],
      );
    }
    else{
      $data = array(
      'kode_barang'=>$this->input->post('kode_barang'),
      'id_univ'=>$this->input->post('id_univ'),
      'type'=>$this->input->post('type'),
      'nama_barang'=>$this->input->post('nama_barang'),
      'tahun'=>$this->input->post('tahun'),
      'foto' => $this->input->post('old'),
      );
    }
    //var_dump($data);
    $this->db->where('id_kode_barang',$id)
       ->update('tb_jenis_inventaris', $data);
    $this->session->set_flashdata('msg_alert', 'Data Jenis Inventaris berhasil diupdate');
  }
  public function hapusGambar($old)
  {
    return array_map('unlink', glob(FCPATH."assets/photoJenisKategori/$old"));
  }
  public function hapusKatInven($id)
  {
    $old = $this->db->select('foto')
            ->from('tb_jenis_inventaris')
            ->where('id_kode_barang',$id)
            ->get();
    $old = $old->row()->foto;
    //var_dump($old);
    $this->hapusGambar($old);
    $this->db->where('id_kode_barang',$id)
       ->delete('tb_jenis_inventaris');
      $this->session->set_flashdata('msg_alert', 'Data Jenis Inventaris berhasil dihapus');
  }

}
