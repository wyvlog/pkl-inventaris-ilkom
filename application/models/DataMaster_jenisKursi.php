<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataMaster_jenisKursi extends CI_Model {

	public function list_all() {
		$q=$this->db->select('jk.*')
					->from('tb_jenis_kursi as jk')
					->get();
		return $q->result();
	}
	public function upload(){
		$config['upload_path'] = './assets/photoKursi/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']	= '2048';
		$config['remove_space'] = TRUE;
		
		$new_name = mt_rand(1000,9999).($_FILES['file_gambar']['name']);
		$config['file_name'] = $new_name;

		$this->load->library('upload', $config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file_gambar')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}
	
	// Fungsi untuk menyimpan data ke database
	public function save($upload){
		$data = array(
			'nama'=>$this->input->post('nama'),
			'foto_kursi' => $upload['file']['file_name'],
		);
		//var_dump($data);
		$this->db->insert('tb_jenis_kursi', $data);
		$this->session->set_flashdata('msg_alert', 'Data Jenis Kursi berhasil ditambahkan');
	}
	public function update($id,$status,$upload)
	{
		if ($status == 1) {
			$data = array(
				'nama'=>$this->input->post('nama'),
				'foto_kursi' => $upload['file']['file_name'],
			);
		}
		else{
			$data = array(
				'nama'=>$this->input->post('nama'),
				'foto_kursi' => $this->input->post('old'),
			);
		}
		//var_dump($data);
		$this->db->where('id_jenis_kursi',$id)
			 ->update('tb_jenis_kursi', $data);
		$this->session->set_flashdata('msg_alert', 'Data Kursi berhasil diupdate');
	}
	public function hapusGambar($old)
	{
		return array_map('unlink', glob(FCPATH."assets/photoKursi/$old"));
	}
	public function hapusJenisKursi($id)
	{
		$old = $this->db->select('foto_kursi')
						->from('tb_jenis_kursi')
						->where('id_jenis_kursi',$id)
						->get();
		$old = $old->row()->foto_kursi;
		//var_dump($old);
		$this->hapusGambar($old);
		$this->db->where('id_jenis_kursi',$id)
			 ->delete('tb_jenis_kursi');
  		$this->session->set_flashdata('msg_alert', 'Data Jenis Kursi berhasil dihapus');
	}
}