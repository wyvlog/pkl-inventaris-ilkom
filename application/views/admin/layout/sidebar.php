<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/admin/dist')?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin Inventaris</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="<?php echo base_url('/Inventaris/KategoriInventaris'); ?>">
            <i class="fa fa-th"></i> <span>Buat Kategori Inventaris</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Upload Inventaris</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li><a href="<?php echo base_url('/Inventaris/listMeja'); ?>"><i class="fa fa-circle-o"></i>Meja</a></li>
            <li><a href="<?=base_url('/Inventaris/listKursi');?>"><i class="fa fa-circle-o"></i>Kursi</a></li>
            <li><a href="<?=base_url('/Inventaris/listPapan');?>"><i class="fa fa-circle-o"></i>Papan Tulis</a></li>
            <li><a href="<?=base_url('/Inventaris/listRak');?>"><i class="fa fa-circle-o"></i>Rak</a></li>
            <li><a href="<?=base_url('/Inventaris/listStapol');?>"><i class="fa fa-circle-o"></i>UPS/Stapol</a></li>
            <li><a href="<?=base_url('/Inventaris/listAC');?>"><i class="fa fa-circle-o"></i>AC</a></li>
            <li><a href="<?=base_url('/Inventaris/listProyektor');?>"><i class="fa fa-circle-o"></i>Proyektor</a></li>
            <li><a href="<?=base_url('/Inventaris/listPrinter');?>"><i class="fa fa-circle-o"></i>Printer</a></li>
            <li><a href="<?=base_url('/Inventaris/listMonitor');?>"><i class="fa fa-circle-o"></i>Monitor</a></li>
            <li><a href="<?=base_url('/Inventaris/listPC');?>"><i class="fa fa-circle-o"></i>PC</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Buat Jenis Inventaris</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url('/Inventaris/listKategoriMeja');?>"><i class="fa fa-circle-o"></i>Jenis Meja</a></li>
            <li><a href="<?=base_url('/Inventaris/listKategoriKursi');?>"><i class="fa fa-circle-o"></i>Jenis Kursi</a></li>
            <li><a href="<?=base_url('/Inventaris/listKategoriRak');?>"><i class="fa fa-circle-o"></i>Jenis Rak</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Peminjaman</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url('/Inventaris/listPeminjamanProyektor');?>"><i class="fa fa-circle-o"></i>Peminjaman Proyektor</a></li>
          </ul>
        </li>
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-circle-o"></i>Report Inventaris</a></li>
          </ul>
        </li>
 -->
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
