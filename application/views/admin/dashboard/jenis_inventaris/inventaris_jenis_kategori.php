<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('admin/layout/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('admin/layout/sidebar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Kategori Inventaris</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="card-body">
                <?php if($this->session->flashdata('msg_alert')) { ?>
                      <div class="alert alert-success">
                          <?=$this->session->flashdata('msg_alert');?>
                      </div>
                <?php } ?>
                <div class="box-header">
                  <h3 class="box-title">Kategori Inventaris
                    <a class="btn btn-flat btn-success btn-sm" id="tambahKelas"><i class="fa fa-plus" ></i></a>
                  </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="dataMakulKurikulum" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Kode Barang</th>
                        <th>Id Universitas</th>
                        <th>Nama Barang</th>
                        <th>Type</th>
                        <th>Tahun</th>
                        <th>Foto</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                     $i = 1;
                     foreach ($kategori as $item){  ?>
                      <tr>
                        <td><?=$i++;?></td>
                        <td><?=$item->kode_barang;?></td>
                        <td><?=$item->id_univ;?></td>
                        <td><?=$item->nama_barang;?></td>
                        <td><?=$item->type;?></td>
                        <td><?=$item->tahun;?></td>
                        <td><img style="width:40px;height:40px;align:center;"src="<?php echo base_url('assets/photoJenisKategori')?>/<?=$item->foto?>" class="img-responsive"</td>
                        <td>
                            <a href="<?=base_url("/Inventaris/hapus/KatInven/{$item->id_kode_barang}");?>" onClick="alert('Yakin Menghapus Kode Inventaris <?=$item->kode_barang;?> ??')" class="btn btn-danger btn-xs" alt="Hapus AC"><i class="fa fa-trash"></i> Hapus</a>
                            <a 
                              data-id_kode_barang="<?=$item->id_kode_barang?>"
                              data-kode_barang = "<?=$item->kode_barang?>"
                              data-id_univ = "<?=$item->id_univ?>"
                              data-type = "<?=$item->type?>"
                              data-tahun = "<?=$item->tahun?>"
                              data-nama_barang = "<?=$item->nama_barang?>"
                              data-old = "<?=$item->foto?>"
                              data-file_gambar = "<?php echo base_url('assets/photoJenisKategori')?>/<?=$item->foto?>"
                            class="btn  btn-warning btn-xs editKatInven"><i class="fa fa-pencil" > Edit</i></a>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <h4 class="modal-title" id="myModalLabel">Kategori Inventaris</h4>
                      </div>
                      <div class="modal-body">

                          <form class="form-horizontal" method="POST" action="<?php echo base_url('Inventaris/addNew/KatInven');?>" enctype="multipart/form-data">
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Kode Barang</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="kode_barang">
                                      <small class="help-block"></small>
                                  </div>
                                </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">ID Universitas</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="id_univ">
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Barang</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="nama_barang">
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Type</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="type">
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Tahun</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="tahun">
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="file_gambar"class="col-md-4 control-label">Foto</label>
                                  <div class="col-md-6">
                                    <div id="imagePreview"></div>
                                    <input type="file" name="file_gambar" id="file" onchange="return fileValidation()" >
                                  </div>
                              </div>   
                              <div class="form-group">
                                  <div class="col-md-6 col-md-offset-4">
                                      <button type="submit" class="btn btn-primary" id="button-reg">
                                          Simpan
                                      </button>
                                  </div>
                              </div>
                          </form>

                      </div>
                  </div>
              </div>
          </div>
          <!--end of Modal -->
          <!-- Modal 2 -->
          <!-- Modal -->
          <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <h4 class="modal-title" id="myModalLabel">Edit Kategori Meja</h4>
                      </div>
                      <div class="modal-body">
           
                          <form class="form-horizontal" method="POST" action="<?= base_url('Inventaris/update/KatInven');?>" enctype="multipart/form-data">
                              <input type="hidden" name="old">
                              <input type="hidden" name="id_kode_barang">
           
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Kode Barang</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="kode_barang">
                                      <small class="help-block"></small>
                                  </div>
                                </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">ID Universitas</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="id_univ">
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Barang</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="nama_barang">
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Type</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="type">
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Tahun</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="tahun">
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="file_gambar"class="col-md-4 control-label">Foto</label>
                                  <div class="col-md-6">
                                    <input type="file" name="file_gambar" id="editfile"  onchange="return validation()" >
                                    <div id="Preview" class="event"><img style="height: 50%; width:40%;" src=""/></div>
                                  </div>
                              </div>   
                              <div class="form-group">
                                  <div class="col-md-6 col-md-offset-4">
                                      <button type="submit" class="btn btn-primary" id="button-reg">
                                          Simpan
                                      </button>
                                  </div>
                              </div>
                          </form>                       
           
                      </div>
                  </div>
              </div>
          </div>
          <!-- End Modal 2 -->    
    </section>
    </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('admin/layout/footer') ?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<?php $this->load->view('admin/layout/scrip') ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="<?=base_url('assets/admin/plugins')?>/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url('assets/admin/plugins')?>/datatables/dataTables.bootstrap.min.js"></script>

     <script type="text/javascript">
      function fileValidation(){

        var fileInput = document.getElementById('file');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if(!allowedExtensions.exec(filePath))
        {
          alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
          fileInput.value = '';
          return false;
        }
        else
        {
         //Image preview
          if (fileInput.files && fileInput.files[0])
          {
            var reader = new FileReader();
            reader.onload = function(e) {
              document.getElementById('imagePreview').innerHTML = '<img style="height: 50%; width:40%;" src="'+e.target.result+'"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
         }
        }
      }
    </script>
    <script type="text/javascript">
      function validation(){

        var fileInput = document.getElementById('editfile');
        var filePath = fileInput.value;
        //alert(filePath);
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if(!allowedExtensions.exec(filePath))
        {
          alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
          fileInput.value = '';
          return false;
        }
        else
        {
         //Image preview
          if (fileInput.files && fileInput.files[0])
          {
            $('#Preview').find('img').remove();
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#Preview').append('<img style="height: 50px; width:40px;" src="'+e.target.result+'"/>');
            };
            reader.readAsDataURL(fileInput.files[0]);
         }
        }
      }
    </script>

    <script>
      $(function () {

        $('#dataMakulKurikulum').DataTable({"pageLength": 10});

         $('#tambahKelas').click(function(){
            $('input+small').text('');
            $('input').parent().removeClass('has-error');
            $('select').parent().removeClass('has-error');

            $('#myModal').modal('show');
            //console.log('test');
            return false;
        });

            $('.editKatInven').click(function(){
            $('input+small').text('');
            $('input').parent().removeClass('has-error');
            $('select').parent().removeClass('has-error');

            $('#myModal2').modal('show');
            
            var form = "#myModal2";
            //$(form).find('select option').removeAttr('selected');
            
            $(form).find('input[name="id_kode_barang"]').val($(this).attr('data-id_kode_barang'));
            $(form).find('input[name="kode_barang"]').val($(this).attr('data-kode_barang'));
            $(form).find('input[name="id_univ"]').val($(this).attr('data-id_univ'));
            $(form).find('input[name="type"]').val($(this).attr('data-type'));
            $(form).find('input[name="nama_barang"]').val($(this).attr('data-nama_barang'));
            $(form).find('input[name="tahun"]').val($(this).attr('data-tahun'));
            $(form).find('input[name="old"]').val($(this).attr('data-old'));
            $(form).find('.event').children('img').attr('src',$(this).attr('data-file_gambar'))

            insert = $(form).find('#formEditKelas').attr('action')+"/"+$(this).attr('data-id_kode_barang');
            $(form).find('#formEditKelas').attr('action',insert);
            //console.log('test');

            return false;
        });

            $(document).on('submit', '#formEditKelas', function(e) {  
            e.preventDefault();
             
            $('input+small').text('');
            $('input').parent().removeClass('has-error');  

             
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                console.log(data);
      
                $('.alert-success').removeClass('hidden');
                $('#myModal2').modal('hide');
                window.location.href=window.location.href; 
            })
            .fail(function(data) {
                console.log(data.responeJSON);
                $.each(data.responseJSON, function (key, value) {
                    var input = '#formEditKelas input[name=' + key + ']';
                    
                    $(input + '+small').text(value);
                    $(input).parent().addClass('has-error');
                });
            });
        });

      });

    </script>

@endsection
