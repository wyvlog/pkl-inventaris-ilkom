<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('admin/layout/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('admin/layout/sidebar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Peminjaman Proyektor</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="card-body">
                <?php if($this->session->flashdata('msg_alert')) { ?>
                      <div class="alert alert-success">
                          <?=$this->session->flashdata('msg_alert');?>
                      </div>
                <?php } ?>
                <div class="box-header">
                  <h3 class="box-title">Peminjaman Proyektor
                    <a class="btn btn-flat btn-success btn-sm" id="tambahKelas"><i class="fa fa-plus" ></i></a>
                    <!-- <a href="<?php //echo base_url('Inventaris/export/ac') ?>" class="btn btn-flat btn-primary btn-sm" target="_blank">Export Table</a> -->
                  </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="dataMakulKurikulum" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Id Peminjaman</th>
                        <th>No Inventaris Proyektor</th>
                        <th>Lokasi Peminjaman</th>
                        <th>Tanggal Peminjaman</th>
                        <th>Tanggal Mengembalikan</th>
                        <th>Nama</th>
                        <th>NIM</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                     $i = 1;
                     foreach ($pinjam as $item){  ?>
                      <tr>
                        <td><?=$item->id_peminjaman;?></td>
                        <td><?=$item->id_proyektor;?></td>
                        <td><?=$item->namaRuangan;?></td>
                        <td><?php echo isset($item->tgl_ambil) ? set_value('tgl_ambil', date('Y-m-d', strtotime($item->tgl_ambil))) : set_value('tgl_ambil'); ?></td>
                        <td><?=$item->tgl_kembali;?></td>
                        <td><?=$item->nama;?></td>
                        <td><?=$item->nim;?></td>
                        <?php if($item->status == 1){ ?>
                        <td>
                          <a href="<?=base_url("/Inventaris/kembalikanProyektor/0/{$item->id_peminjaman}");?>" onClick="alert('Yakin Merubah Status Peminjaman??')" class="btn btn-success btn-xs">Dikembalikan <i class="fa fa-check"></i></a>
                        </td>
                        <?php }else{ ?>
                        <td>
                          <a href="<?=base_url("/Inventaris/kembalikanProyektor/1/{$item->id_peminjaman}");?>" onClick="alert('Yakin Merubah Status Peminjaman??')" class="btn btn-danger btn-xs">Belum Dikembalikan <i class="fa fa-question"></i></a>
                        </td>
                        <?php } ?>
                        <td>
                            <a href="<?=base_url("/Inventaris/hapus/peminjamanProyektor/{$item->id_peminjaman}");?>" onClick="alert('Yakin Menghapus Kode Inventaris <?=$item->id_peminjaman;?> ??')" class="btn btn-danger btn-xs" alt="Hapus AC"><i class="fa fa-trash"></i> Hapus</a>
                            <a 
                              data-id_peminjaman="<?=$item->id_peminjaman?>"
                              data-id_proyektor = "<?=$item->id_proyektor?>"
                              data-id_ruangan = "<?=$item->id_ruangan?>"
                              data-tgl_ambil = "<?=$item->tgl_ambil?>"
                              data-tgl_kembali = "<?=$item->tgl_kembali?>"
                              data-nama = "<?=$item->nama?>"
                              data-nim = "<?=$item->nim?>"
                            class="btn  btn-warning btn-xs editKatInven"><i class="fa fa-pencil" > Edit</i></a>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <h4 class="modal-title" id="myModalLabel">Peminjaman Proyektor</h4>
                      </div>
                      <div class="modal-body">

                          <form class="form-horizontal" method="POST" action="<?php echo base_url('Inventaris/addNew/pinjamProyektor');?>" enctype="multipart/form-data">
                              <div class="form-group">
                                  <label class="col-md-4 control-label">No Inventaris Proyektor</label>
                                  <div class="col-md-6 has-error">
                                      <select class="form-control" name="id_proyektor" >
                                          <?php  foreach ($proyektor as $data) { ?>
                                            <option value="<?=$data->id_proyektor?>"><?=$data->id_proyektor?></option>
                                          <?php } ?>
                                      </select>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Tanggal Meminjam</label>
                                  <div class="col-md-6 has-error">
                                      <input type="date" class="form-control" name="tgl_ambil" required>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Gedung</label>
                                  <div class="col-md-6 has-error">
                                      <select class="form-control" name="gedung" id="id_gedung" >
                                        <option value="0" disable="true" selected="true">=== PILIH Gedung ===</option>
                                          <?php foreach ($gedung as $data) {?>
                                          <option value="<?=$data->id_gedung?>"><?=$data->nama?></option>
                                          <?php } ?>
                                      </select>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Ruangan</label>
                                  <div class="col-md-6 has-error">
                                      <select class="form-control" name="ruangan" id="ruangan">
                                         <option value="0" disable="true" selected="true">=== PILIH ===</option>
                                      </select>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">NIM Peminjam</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="nim" required>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Peminjam</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="nama" required>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="col-md-6 col-md-offset-4">
                                      <button type="submit" class="btn btn-primary" id="button-reg">
                                          Simpan
                                      </button>
                                  </div>
                              </div>
                          </form>

                      </div>
                  </div>
              </div>
          </div>
          <!--end of Modal -->
          <!-- Modal 2 -->
          <!-- Modal -->
          <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <h4 class="modal-title" id="myModalLabel">Edit Peminjaman Proyektor</h4>
                      </div>
                      <div class="modal-body">
           
                          <form class="form-horizontal" method="POST" action="<?= base_url('Inventaris/update/pinjamProyektor');?>" enctype="multipart/form-data">
                            <input type="hidden" name="id_peminjaman">
                              <div class="form-group">
                                  <label class="col-md-4 control-label">No Inventaris Proyektor</label>
                                  <div class="col-md-6 has-error">
                                      <select class="form-control" name="id_proyektor" >
                                          <?php  foreach ($proyektor as $data) { ?>
                                            <option value="<?=$data->id_proyektor?>"><?=$data->id_proyektor?></option>
                                          <?php } ?>
                                      </select>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Tanggal Meminjam</label>
                                  <div class="col-md-6 has-error">
                                      <input type="date" class="form-control" name="tgl_ambil" required>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Gedung</label>
                                  <div class="col-md-6 has-error">
                                      <select class="form-control" name="gedung" id="gedung" >
                                        <option value="0" disable="true" selected="true">=== PILIH Gedung ===</option>
                                          <?php foreach ($gedung as $data) {?>
                                          <option value="<?=$data->id_gedung?>"><?=$data->nama?></option>
                                          <?php } ?>
                                      </select>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Ruangan</label>
                                  <div class="col-md-6 has-error">
                                      <select class="form-control" name="id_ruangan" id="id_ruangan">
                                         <option value="0" disable="true" selected="true">=== PILIH ===</option>
                                      </select>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">NIM Peminjam</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="nim" required>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Peminjam</label>
                                  <div class="col-md-6 has-error">
                                      <input type="text" class="form-control" name="nama" required>
                                      <small class="help-block"></small>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="col-md-6 col-md-offset-4">
                                      <button type="submit" class="btn btn-primary" id="button-reg">
                                          Simpan
                                      </button>
                                  </div>
                              </div>
                          </form>                       
           
                      </div>
                  </div>
              </div>
          </div>
          <!-- End Modal 2 -->    
    </section>
    </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('admin/layout/footer') ?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<?php $this->load->view('admin/layout/scrip') ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="<?=base_url('assets/admin/plugins')?>/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url('assets/admin/plugins')?>/datatables/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        $('#id_gedung').change(function(){
          var gedung = $(this).val();

          //alert(gedung);

          $.ajax({
            url:'<?=base_url()?>Inventaris/ruangan',
            method: 'post',
            data: {gedung: gedung},
            dataType: 'json',
            success: function(response){


              // Remove options
              $('#ruangan').find('option').not(':first').remove();

              // Add options
              $.each(response,function(index,data){
                 $('#ruangan').append('<option value="'+data['id_ruangan']+'">'+data['nama']+'</option>');
              });
            }
          });
        });
      });

    </script>
        <script type="text/javascript">
      $(document).ready(function(){
        $('#gedung').change(function(){
          var gedung = $(this).val();

          //alert(gedung);

          $.ajax({
            url:'<?=base_url()?>Inventaris/ruangan',
            method: 'post',
            data: {gedung: gedung},
            dataType: 'json',
            success: function(response){


              // Remove options
              $('#id_ruangan').find('option').not(':first').remove();

              // Add options
              $.each(response,function(index,data){
                 $('#id_ruangan').append('<option value="'+data['id_ruangan']+'">'+data['nama']+'</option>');
              });
            }
          });
        });
      });

    </script>
     <script type="text/javascript">
      function fileValidation(){

        var fileInput = document.getElementById('file');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if(!allowedExtensions.exec(filePath))
        {
          alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
          fileInput.value = '';
          return false;
        }
        else
        {
         //Image preview
          if (fileInput.files && fileInput.files[0])
          {
            var reader = new FileReader();
            reader.onload = function(e) {
              document.getElementById('imagePreview').innerHTML = '<img style="height: 50%; width:40%;" src="'+e.target.result+'"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
         }
        }
      }
    </script>
    <script type="text/javascript">
      function validation(){

        var fileInput = document.getElementById('editfile');
        var filePath = fileInput.value;
        //alert(filePath);
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if(!allowedExtensions.exec(filePath))
        {
          alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
          fileInput.value = '';
          return false;
        }
        else
        {
         //Image preview
          if (fileInput.files && fileInput.files[0])
          {
            $('#Preview').find('img').remove();
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#Preview').append('<img style="height: 50px; width:40px;" src="'+e.target.result+'"/>');
            };
            reader.readAsDataURL(fileInput.files[0]);
         }
        }
      }
    </script>

    <script>
      $(function () {

        $('#dataMakulKurikulum').DataTable({"pageLength": 10});

         $('#tambahKelas').click(function(){
            $('input+small').text('');
            $('input').parent().removeClass('has-error');
            $('select').parent().removeClass('has-error');

            $('#myModal').modal('show');
            //console.log('test');
            return false;
        });

            $('.editKatInven').click(function(){
            $('input+small').text('');
            $('input').parent().removeClass('has-error');
            $('select').parent().removeClass('has-error');

            $('#myModal2').modal('show');
            
            var form = "#myModal2";
            //$(form).find('select option').removeAttr('selected');
            
            $(form).find('input[name="id_peminjaman"]').val($(this).attr('data-id_peminjaman'));
            $(form).find('input[name="id_proyektor"]').val($(this).attr('data-id_proyektor'));
            $(form).find('input[name="ruangan"]').val($(this).attr('data-id_ruangan'));
            $(form).find('input[name="tgl_ambil"]').val($(this).attr('data-tgl_ambil'));
            $(form).find('input[name="nama"]').val($(this).attr('data-nama'));
            $(form).find('input[name="nim"]').val($(this).attr('data-nim'));
          
            insert = $(form).find('#formEditKelas').attr('action')+"/"+$(this).attr('data-id_peminjaman');
            $(form).find('#formEditKelas').attr('action',insert);
            //console.log('test');

            return false;
        });

            $(document).on('submit', '#formEditKelas', function(e) {  
            e.preventDefault();
             
            $('input+small').text('');
            $('input').parent().removeClass('has-error');  

             
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                console.log(data);
      
                $('.alert-success').removeClass('hidden');
                $('#myModal2').modal('hide');
                window.location.href=window.location.href; 
            })
            .fail(function(data) {
                console.log(data.responeJSON);
                $.each(data.responseJSON, function (key, value) {
                    var input = '#formEditKelas input[name=' + key + ']';
                    
                    $(input + '+small').text(value);
                    $(input).parent().addClass('has-error');
                });
            });
        });

      });

    </script>

@endsection
