<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('admin/layout/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('admin/layout/sidebar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
        <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Bar Chart Data Inventaris</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>

        <div class="col-md-6">
          <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Chart Data Inventaris</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col (LEFT) -->
        <div class="col-md-6">
          <!-- LINE CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Line Chart Data Kursi</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="line-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('admin/layout/footer') ?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('admin/layout/scrip') ?>
</body>
<script>
  $(function () {
    "use strict";
    // LINE CHART
    var line = new Morris.Line({
      element: 'line-chart',
      resize: true,
      data: [
      <?php foreach ($tahun as $data) {?>
        {y: '<?=$data->tahun?>', inventaris: <?=$data->total?>},
      <?php } ?>
      ],
      xkey: 'y',
      ykeys: ['inventaris'],
      labels: ['Total Inventaris Ilkom'],
      lineColors: ['#3c8dbc'],
      hideHover: 'auto'
    });

    //DONUT CHART
    var donut = new Morris.Donut({
      element: 'sales-chart',
      resize: true,
      colors: ["#00a65a", "#f56954"],
      data: [
        {label: "Inventaris Kondisi Baik", value: <?=$Benar ?>},
        {label: "Inventaris Kondisi Rusak", value: <?=$Rusak ?>}
      ],
      hideHover: 'auto'
    });
    //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart',
      resize: true,
      data: [
        {y: 'Kursi', a: <?=$kursiBenar ?>, b: <?=$kursiRusak ?>},
        {y: 'AC', a: <?=$acBenar ?>, b: <?=$acRusak ?>},
        {y: 'PC', a: <?=$pcBenar ?>, b: <?=$pcRusak ?>},
        {y: 'Meja', a: <?=$mejaBenar ?>, b: <?=$mejaRusak ?>},
        {y: 'Proyektor', a: <?=$proyektorBenar ?>, b: <?=$proyektorRusak ?>},
        {y: 'Rak', a: <?=$rakBenar ?>, b: <?=$rakRusak ?>},
        {y: 'Monitor', a: <?=$monitorBenar ?>, b: <?=$monitorRusak ?>},
        {y: 'Printer', a: <?=$printerBenar ?>, b: <?=$printerRusak ?>},
        {y: 'Papan', a: <?=$papanBenar ?>, b: <?=$papanRusak ?>},
        {y: 'Stapol', a: <?=$stapolBenar ?>, b: <?=$stapolRusak ?>},
        {y: 'Arsip', a: <?=$arsipBenar ?>, b: <?=$arsipRusak ?>},
      ],
      barColors: ['#00a65a', '#f56954'],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['Baik', 'Rusak'],
      hideHover: 'auto'
    });
  });
</script>
</html>
