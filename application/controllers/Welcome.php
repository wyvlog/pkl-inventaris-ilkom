<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$this->load->model('Dashboard');
	
		$this->md_dash = $this->Dashboard;
	}
	public function index()
	{
		$data['KursiB'] = $this->db->select('COUNT(*) as total')
					->from('tb_kursi')
					->where('status','1')
					->get();
		$data['KursiR'] = $this->db->select('COUNT(*) as total')
					->from('tb_kursi')
					->where('status','0')
					->get();
		$data['acB'] = $this->db->select('COUNT(*) as total')
					->from('tb_ac')
					->where('status','1')
					->get();
		$data['acR'] = $this->db->select('COUNT(*) as total')
					->from('tb_ac')
					->where('status','0')
					->get();
		$data['pcB'] = $this->db->select('COUNT(*) as total')
					->from('tb_pc')
					->where('status','1')
					->get();
		$data['pcR'] = $this->db->select('COUNT(*) as total')
					->from('tb_pc')
					->where('status','0')
					->get();
		$data['mejaB'] = $this->db->select('COUNT(*) as total')
					->from('tb_meja')
					->where('status','1')
					->get();
		$data['mejaR'] = $this->db->select('COUNT(*) as total')
					->from('tb_meja')
					->where('status','0')
					->get();
		$data['proyektorB'] = $this->db->select('COUNT(*) as total')
					->from('tb_proyektor')
					->where('status','1')
					->get();
		$data['proyektorR'] = $this->db->select('COUNT(*) as total')
					->from('tb_proyektor')
					->where('status','0')
					->get();
		$data['rakB'] = $this->db->select('COUNT(*) as total')
					->from('tb_rak')
					->where('status','1')
					->get();
		$data['rakR'] = $this->db->select('COUNT(*) as total')
					->from('tb_rak')
					->where('status','0')
					->get();
		$data['monitorB'] = $this->db->select('COUNT(*) as total')
					->from('tb_monitor')
					->where('status','1')
					->get();
		$data['monitorR'] = $this->db->select('COUNT(*) as total')
					->from('tb_monitor')
					->where('status','0')
					->get();
		$data['printerB'] = $this->db->select('COUNT(*) as total')
					->from('tb_printer')
					->where('status','1')
					->get();
		$data['printerR'] = $this->db->select('COUNT(*) as total')
					->from('tb_printer')
					->where('status','0')
					->get();
		$data['papanB'] = $this->db->select('COUNT(*) as total')
					->from('tb_papan')
					->where('status','1')
					->get();
		$data['papanR'] = $this->db->select('COUNT(*) as total')
					->from('tb_papan')
					->where('status','0')
					->get();
		$data['stapolB'] = $this->db->select('COUNT(*) as total')
					->from('tb_stapol')
					->where('status','1')
					->get();
		$data['stapolR'] = $this->db->select('COUNT(*) as total')
					->from('tb_stapol')
					->where('status','0')
					->get();
		$data['arsipB'] = $this->db->select('COUNT(*) as total')
					->from('tb_arsip')
					->where('status','1')
					->get();
		$data['arsipR'] = $this->db->select('COUNT(*) as total')
					->from('tb_arsip')
					->where('status','0')
					->get();

		$data['kursiBenar'] = $data['KursiB']->row()->total;
		$data['kursiRusak'] = $data['KursiR']->row()->total;
		$data['acBenar'] = $data['acB']->row()->total;
		$data['acRusak'] = $data['acR']->row()->total;
		$data['pcBenar'] = $data['pcB']->row()->total;
		$data['pcRusak'] = $data['pcR']->row()->total;
		$data['mejaBenar'] = $data['mejaB']->row()->total;
		$data['mejaRusak'] = $data['mejaR']->row()->total;
		$data['proyektorBenar'] = $data['proyektorB']->row()->total;
		$data['proyektorRusak'] = $data['proyektorR']->row()->total;
		$data['rakBenar'] = $data['proyektorB']->row()->total;
		$data['rakRusak'] = $data['proyektorR']->row()->total;
		$data['monitorBenar'] = $data['monitorB']->row()->total;
		$data['monitorRusak'] = $data['monitorR']->row()->total;
		$data['printerBenar'] = $data['printerB']->row()->total;
		$data['printerRusak'] = $data['printerR']->row()->total;
		$data['papanBenar'] = $data['papanB']->row()->total;
		$data['papanRusak'] = $data['papanR']->row()->total;
		$data['stapolBenar'] = $data['stapolB']->row()->total;
		$data['stapolRusak'] = $data['stapolR']->row()->total;
		$data['arsipBenar'] = $data['arsipB']->row()->total;
		$data['arsipRusak'] = $data['arsipR']->row()->total;

		$data['Benar'] = $data['kursiBenar']+$data['acBenar']+$data['pcBenar']+$data['mejaBenar']+$data['proyektorBenar']+$data['rakBenar']+$data['monitorBenar']+$data['printerBenar']+$data['papanBenar']+$data['stapolBenar']+$data['arsipBenar'];
		$data['Rusak'] = $data['kursiRusak']+$data['acRusak']+$data['pcRusak']+$data['mejaRusak']+$data['proyektorRusak']+$data['rakRusak']+$data['monitorRusak']+$data['printerRusak']+$data['papanRusak']+$data['stapolRusak']+$data['arsipRusak'];


		$data['tahun'] = $this->md_dash->tahun();

		$data['test'] = $this->db->select('tahun, COUNT(tahun) as total')
					->from('tb_kursi as k')
					->join('tb_jenis_inventaris as ji','k.id_kode_barang = ji.id_kode_barang')
					->group_by('tahun')
					->get();


		// $data['kusi'] = $this->md_dash->kursi('tb_kursi');
		// var_dump($data['tahun']);
		$this->load->view('admin/dashboard/index',$data);
	}
}
