<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventaris extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$this->load->model('DataMaster_Kursi');
		$this->load->model('DataMaster_AC');
		$this->load->model('DataMaster_Meja');
		$this->load->model('DataMaster_jenisMeja');
		$this->load->model('DataMaster_jenisKursi');
		$this->load->model('DataMaster_jenisRak');
		$this->load->model('DataMaster_PC');
		$this->load->model('DataMaster_Rak');
		$this->load->model('DataMaster_Stapol');
		$this->load->model('DataMaster_papanTulis');
		$this->load->model('DataMaster_Printer');
		$this->load->model('DataMaster_Proyektor');
		$this->load->model('DataMaster_Monitor');

		$this->load->model('DataMaster_KatInven');
		$this->load->model('DataMaster_PeminjamanProyektor');
		$this->md_kursi = $this->DataMaster_Kursi;
		$this->md_jMeja = $this->DataMaster_jenisMeja;
		$this->md_jKursi = $this->DataMaster_jenisKursi;
		$this->md_jRak = $this->DataMaster_jenisRak;
		$this->md_ac = $this->DataMaster_AC;
		$this->md_meja = $this->DataMaster_Meja;
		$this->md_pc = $this->DataMaster_PC;
		$this->md_papan = $this->DataMaster_papanTulis;
		$this->md_rak = $this->DataMaster_Rak;
		$this->md_stapol = $this->DataMaster_Stapol;
		$this->md_printer = $this->DataMaster_Printer;
		$this->md_proyektor = $this->DataMaster_Proyektor;
		$this->md_monitor = $this->DataMaster_Monitor;
		$this->md_katInven = $this->DataMaster_KatInven;
		$this->md_pinjamProyektor = $this->DataMaster_PeminjamanProyektor;
		// $this->load->model('DataMaster_MataKuliah');
		// $this->md_mk = $this->DataMaster_MataKuliah;
	}
	public function index()
	{
		redirect( base_url() );
	}
	public function KategoriInventaris()
	{
		$data['kategori'] = $this->md_katInven->list_all();
		$this->load->view('admin/dashboard/jenis_inventaris/inventaris_jenis_kategori',$data);
	}
	public function listKategoriMeja()
	{
		$data['jenis'] = $this->md_jMeja->list_all();
		//var_dump($data);
		$this->load->view('admin/dashboard/kategori/kategoriMeja',$data);
	}
	public function listKategoriKursi()
	{
		$data['jenis'] = $this->md_jKursi->list_all();
		//var_dump($data);
		$this->load->view('admin/dashboard/kategori/kategoriKursi',$data);
	}

	public function listKategoriRak()
	{
		$data['jenis'] = $this->md_jRak->list_all();
		//var_dump($data);
		$this->load->view('admin/dashboard/kategori/kategoriRak',$data);
	}
	public function listKursi()
	{
		$data['inven'] = $this->md_kursi->list_all();
		$data['gedung'] = $this->md_kursi->gedung();
		$data['jenis'] = $this->md_kursi->jenis();
		$data['cor'] = $this->md_kursi->tb_jenis_inventaris();
		//var_dump($data);
		$this->load->view('admin/dashboard/inventory/inventarisKursi',$data);
	}

	public function listPC(){

		$data['inven'] = $this->md_pc->list_all();
		$data['gedung'] = $this->md_pc->gedung();
		$data['cor'] = $this->md_pc->tb_jenis_inventaris();
		$this->load->view('admin/dashboard/inventory/inventaris_pc',$data);
	}

	public function listAC()
	{
		$data['inven'] = $this->md_ac->list_all();
		$data['gedung'] = $this->md_ac->gedung();
		$data['cor'] = $this->md_ac->tb_jenis_inventaris();

		$this->load->view('admin/dashboard/inventory/inventaris_ac',$data);

	}


	public function export(){
		if( empty($this->uri->segment('3'))) {
			redirect( base_url() );
		}

		$cek=$this->uri->segment('3');
		//var_dump($cek)


		switch ($cek) {
			case 'ac' :

				$data['inven'] = $this->md_ac->list_all();

				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("idr corner");
				$objPHPExcel->getProperties()->setLastModifiedBy("idr corner");
				$objPHPExcel->getProperties()->setTitle("Data Ac");


				$objPHPExcel->setActiveSheetIndex(0);

				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			        )

			    );
			    $styleColor = array(
			    	'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '5a6e8f')
			        )
			    );
			    $BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_THICK,
				      'color' => array('rgb' => '5a6e8f')
				    )
				  )
				);
			 	for($cells = "A"; $cells <= "F"; $cells++){
			 		$objPHPExcel->getActiveSheet()->getColumnDimension($cells)->setAutoSize(true);
			 		$objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($style);
			 	}
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($styleColor);
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold( true )->getColor()->setRGB('FFFFFF');

				$objPHPExcel->getActiveSheet()->setCellValue('A1','Nomor');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Id AC');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Tipe');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Tahun');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Lokasi');
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Kondisi');


				$baris = 2;
				$x=1;

				foreach ($data['inven'] as $item) {
					# code...
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris, $x);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris, $item->id_ac);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris, $item->type);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris, $item->tahun);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, $item->ruangan, $item->gedung);
					if($item->status == 1){
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Layak");
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Tidak Layak");
					}
					$baris++;
					$x++;
				}
				$objPHPExcel->getActiveSheet()->getStyle("A1".":F".$x)->applyFromArray($BStyle);
				$filename = "Data Ac".date("d-m-Y-H-i-s").'.xlsx';
				$objPHPExcel->getActiveSheet()->setTitle("Data Ac");

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age = 0');
				$writer = PHPExcel_IOFactory::createwriter($objPHPExcel, 'Excel2007');
				$writer->save('php://output');
				exit;

			break;



			case 'papan' :

				$data['inven'] = $this->md_papan->list_all();

				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("idr corner");
				$objPHPExcel->getProperties()->setLastModifiedBy("idr corner");
				$objPHPExcel->getProperties()->setTitle("Data Ac");


				$objPHPExcel->setActiveSheetIndex(0);

				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			        )

			    );
			    $styleColor = array(
			    	'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '5a6e8f')
			        )
			    );
			    $BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_THICK,
				      'color' => array('rgb' => '5a6e8f')
				    )
				  )
				);
			 	for($cells = "A"; $cells <= "F"; $cells++){
			 		$objPHPExcel->getActiveSheet()->getColumnDimension($cells)->setAutoSize(true);
			 		$objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($style);
			 	}
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($styleColor);
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold( true )->getColor()->setRGB('FFFFFF');

				$objPHPExcel->getActiveSheet()->setCellValue('A1','Nomor');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Id Papan');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Tipe');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Tahun');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Lokasi');
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Kondisi');


				$baris = 2;
				$x=1;

				foreach ($data['inven'] as $item) {
					# code...
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris, $x);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris, $item->id_papan);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris, $item->type);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris, $item->tahun);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, $item->ruangan, $item->gedung);
					if($item->status == 1){
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Layak");
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Tidak Layak");
					}
					$baris++;
					$x++;
				}
				$objPHPExcel->getActiveSheet()->getStyle("A1".":F".$x)->applyFromArray($BStyle);
				$filename = "Data Papan Tulis ".date("d-m-Y-H-i-s").'.xlsx';
				$objPHPExcel->getActiveSheet()->setTitle("Data Papan Tulis");

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age = 0');
				$writer = PHPExcel_IOFactory::createwriter($objPHPExcel, 'Excel2007');
				$writer->save('php://output');
				exit;

			break;



			case 'meja' :

				$data['inven'] = $this->md_meja->list_all();

				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("idr corner");
				$objPHPExcel->getProperties()->setLastModifiedBy("idr corner");
				$objPHPExcel->getProperties()->setTitle("Data Meja");


				$objPHPExcel->setActiveSheetIndex(0);


				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			        )

			    );
			    $styleColor = array(
			    	'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '5a6e8f')
			        )
			    );
			    $BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_THICK,
				      'color' => array('rgb' => '5a6e8f')
				    )
				  )
				);
			 	for($cells = "A"; $cells <= "G"; $cells++){
			 		$objPHPExcel->getActiveSheet()->getColumnDimension($cells)->setAutoSize(true);
			 		$objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($style);
			 	}
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->applyFromArray($styleColor);
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold( true )->getColor()->setRGB('FFFFFF');

				$objPHPExcel->getActiveSheet()->setCellValue('A1','Nomor');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Id Meja');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Jenis Meja');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Tipe');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Tahun');
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Lokasi');
				$objPHPExcel->getActiveSheet()->setCellValue('G1','Kondisi');


				$baris = 2;
				$x=1;

				foreach ($data['inven'] as $item) {
					# code...
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris, $x);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris, $item->id_meja);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris, $item->jenisMeja);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris, $item->tipeMeja);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, $item->tahun);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, $item->ruangan, $item->gedung);
					if($item->status == 1){
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$baris, "Layak");
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$baris, "Tidak Layak");
					}
					$baris++;
					$x++;
				}
				$objPHPExcel->getActiveSheet()->getStyle("A1".":G".$x)->applyFromArray($BStyle);
				$filename = "Data Meja".date("d-m-Y-H-i-s").'.xlsx';
				$objPHPExcel->getActiveSheet()->setTitle("Data Meja");

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age = 0');
				$writer = PHPExcel_IOFactory::createwriter($objPHPExcel, 'Excel2007');
				$writer->save('php://output');
				exit;

			break;


			case 'kursi' :

				$data['inven'] = $this->md_kursi->list_all();

				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("idr corner");
				$objPHPExcel->getProperties()->setLastModifiedBy("idr corner");
				$objPHPExcel->getProperties()->setTitle("Data Kursi");


				$objPHPExcel->setActiveSheetIndex(0);

				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			        )

			    );
			    $styleColor = array(
			    	'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '5a6e8f')
			        )
			    );
			    $BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_THICK,
				      'color' => array('rgb' => '5a6e8f')
				    )
				  )
				);
			 	for($cells = "A"; $cells <= "G"; $cells++){
			 		$objPHPExcel->getActiveSheet()->getColumnDimension($cells)->setAutoSize(true);
			 		$objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($style);
			 	}
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->applyFromArray($styleColor);
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold( true )->getColor()->setRGB('FFFFFF');


				$objPHPExcel->getActiveSheet()->setCellValue('A1','Nomor');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Id Kursi');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Jenis Kursi');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Tipe');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Tahun');
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Lokasi');
				$objPHPExcel->getActiveSheet()->setCellValue('G1','Kondisi');


				$baris = 2;
				$x=1;

				foreach ($data['inven'] as $item) {
					# code...
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris, $x);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris, $item->id_kursi);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris, $item->jenisKursi);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris, $item->type);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, $item->tahun);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, $item->ruangan, $item->gedung);
					if($item->status == 1){
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$baris, "Layak");
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$baris, "Tidak Layak");
					}
					$baris++;
					$x++;
				}
				$objPHPExcel->getActiveSheet()->getStyle("A1".":G".$x)->applyFromArray($BStyle);
				$filename = "Data Kursi ".date("d-m-Y-H-i-s").'.xlsx';
				$objPHPExcel->getActiveSheet()->setTitle("Data Kursi");

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age = 0');
				$writer = PHPExcel_IOFactory::createwriter($objPHPExcel, 'Excel2007');
				$writer->save('php://output');
				exit;

			break;


			case 'rak' :

				$data['inven'] = $this->md_rak->list_all();

				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("idr corner");
				$objPHPExcel->getProperties()->setLastModifiedBy("idr corner");
				$objPHPExcel->getProperties()->setTitle("Data Rak");



				$objPHPExcel->setActiveSheetIndex(0);

				// $cells = array('A','B','C','D','E','F','G');

				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			        )

			    );
			    $styleColor = array(
			    	'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '5a6e8f')
			        )
			    );
			    $BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_THICK,
				      'color' => array('rgb' => '5a6e8f')
				    )
				  )
				);
			 	for($cells = "A"; $cells <= "G"; $cells++){
			 		$objPHPExcel->getActiveSheet()->getColumnDimension($cells)->setAutoSize(true);
			 		$objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($style);
			 	}
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->applyFromArray($styleColor);
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold( true )->getColor()->setRGB('FFFFFF');


				$objPHPExcel->getActiveSheet()->setCellValue('A1','Nomor');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Id Rak');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Jenis Rak');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Tipe');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Tahun');
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Lokasi');
				$objPHPExcel->getActiveSheet()->setCellValue('G1','Kondisi');



				$baris = 2;
				$x=1;

				foreach ($data['inven'] as $item) {
					# code...
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris, $x);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris, $item->id_rak);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris, $item->jenisRak);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris, $item->tipeRak);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, $item->tahun);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, $item->ruangan, $item->gedung);
					if($item->status == 1){
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$baris, "Layak");
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$baris, "Tidak Layak");
					}
					$baris++;
					$x++;
				}
				$objPHPExcel->getActiveSheet()->getStyle("A1".":G".$x)->applyFromArray($BStyle);
				$filename = "Data Rak ".date("d-m-Y-H-i-s").'.xlsx';
				$objPHPExcel->getActiveSheet()->setTitle("Data Rak");

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age = 0');
				$writer = PHPExcel_IOFactory::createwriter($objPHPExcel, 'Excel2007');
				$writer->save('php://output');
				exit;

			break;


			case 'stapol' :

				$data['inven'] = $this->md_stapol->list_all();

				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("idr corner");
				$objPHPExcel->getProperties()->setLastModifiedBy("idr corner");
				$objPHPExcel->getProperties()->setTitle("Data UPS/Stapol");


				$objPHPExcel->setActiveSheetIndex(0);

				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			        )

			    );
			    $styleColor = array(
			    	'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '5a6e8f')
			        )
			    );
			    $BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_THICK,
				      'color' => array('rgb' => '5a6e8f')
				    )
				  )
				);
			 	for($cells = "A"; $cells <= "F"; $cells++){
			 		$objPHPExcel->getActiveSheet()->getColumnDimension($cells)->setAutoSize(true);
			 		$objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($style);
			 	}
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($styleColor);
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold( true )->getColor()->setRGB('FFFFFF');

				$objPHPExcel->getActiveSheet()->setCellValue('A1','Nomor');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Id UPS/Stapol');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Tipe');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Tahun');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Lokasi');
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Kondisi');


				$baris = 2;
				$x=1;

				foreach ($data['inven'] as $item) {
					# code...
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris, $x);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris, $item->id_stapol);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris, $item->type);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris, $item->tahun);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, $item->ruangan, $item->gedung);
					if($item->status == 1){
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Layak");
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Tidak Layak");
					}
					$baris++;
					$x++;
				}
				$objPHPExcel->getActiveSheet()->getStyle("A1".":F".$x)->applyFromArray($BStyle);
				$filename = "Data UPS/Stapol".date("d-m-Y-H-i-s").'.xlsx';
				$objPHPExcel->getActiveSheet()->setTitle("Data Stapol");

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age = 0');
				$writer = PHPExcel_IOFactory::createwriter($objPHPExcel, 'Excel2007');
				$writer->save('php://output');
				exit;

			break;




			case 'proyektor' :

			$data['inven'] = $this->md_proyektor->list_all();

				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("idr corner");
				$objPHPExcel->getProperties()->setLastModifiedBy("idr corner");
				$objPHPExcel->getProperties()->setTitle("Data Proyektor");


				$objPHPExcel->setActiveSheetIndex(0);


				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			        )

			    );
			    $styleColor = array(
			    	'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '5a6e8f')
			        )
			    );
			    $BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_THICK,
				      'color' => array('rgb' => '5a6e8f')
				    )
				  )
				);
			 	for($cells = "A"; $cells <= "E"; $cells++){
			 		$objPHPExcel->getActiveSheet()->getColumnDimension($cells)->setAutoSize(true);
			 		$objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($style);
			 	}
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:E1")->applyFromArray($styleColor);
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:E1")->getFont()->setBold( true )->getColor()->setRGB('FFFFFF');

				$objPHPExcel->getActiveSheet()->setCellValue('A1','Nomor');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Id Proyektor');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Tipe');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Tahun');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Kondisi');


				$baris = 2;
				$x=1;

				foreach ($data['inven'] as $item) {
					# code...
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris, $x);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris, $item->id_proyektor);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris, $item->type);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris, $item->tahun);
					if($item->status == 1){
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, "Layak");
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, "Tidak Layak");
					}
					$baris++;
					$x++;
				}
				$objPHPExcel->getActiveSheet()->getStyle("A1".":E".$x)->applyFromArray($BStyle);
				$filename = "Data Proyektor".date("d-m-Y-H-i-s").'.xlsx';
				$objPHPExcel->getActiveSheet()->setTitle("Data Proyektor");

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age = 0');
				$writer = PHPExcel_IOFactory::createwriter($objPHPExcel, 'Excel2007');
				$writer->save('php://output');
				exit;

				break;




			case 'printer' :

			$data['inven'] = $this->md_printer->list_all();

				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("idr corner");
				$objPHPExcel->getProperties()->setLastModifiedBy("idr corner");
				$objPHPExcel->getProperties()->setTitle("Data Printer");


				$objPHPExcel->setActiveSheetIndex(0);

				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			        )

			    );
			    $styleColor = array(
			    	'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '5a6e8f')
			        )
			    );
			    $BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_THICK,
				      'color' => array('rgb' => '5a6e8f')
				    )
				  )
				);
			 	for($cells = "A"; $cells <= "F"; $cells++){
			 		$objPHPExcel->getActiveSheet()->getColumnDimension($cells)->setAutoSize(true);
			 		$objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($style);
			 	}
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($styleColor);
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold( true )->getColor()->setRGB('FFFFFF');


				$objPHPExcel->getActiveSheet()->setCellValue('A1','Nomor');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Id Printer');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Tipe');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Tahun');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Ruangan');
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Kondisi');


				$baris = 2;
				$x=1;

				foreach ($data['inven'] as $item) {
					# code...
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris, $x);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris, $item->id_printer);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris, $item->type);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris, $item->tahun);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, $item->ruangan, $item->gedung);
					if($item->status == 1){
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Layak");
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Tidak Layak");
					}
					$baris++;
					$x++;
				}
				$objPHPExcel->getActiveSheet()->getStyle("A1".":F".$x)->applyFromArray($BStyle);
				$filename = "Data Printer".date("d-m-Y-H-i-s").'.xlsx';
				$objPHPExcel->getActiveSheet()->setTitle("Data Printer");

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age = 0');
				$writer = PHPExcel_IOFactory::createwriter($objPHPExcel, 'Excel2007');
				$writer->save('php://output');
				exit;

				break;



				case 'monitor' :

				$data['inven'] = $this->md_monitor->list_all();

				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("idr corner");
				$objPHPExcel->getProperties()->setLastModifiedBy("idr corner");
				$objPHPExcel->getProperties()->setTitle("Data Monitor");


				$objPHPExcel->setActiveSheetIndex(0);

				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			        )

			    );
			    $styleColor = array(
			    	'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '5a6e8f')
			        )
			    );
			    $BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_THICK,
				      'color' => array('rgb' => '5a6e8f')
				    )
				  )
				);
			 	for($cells = "A"; $cells <= "F"; $cells++){
			 		$objPHPExcel->getActiveSheet()->getColumnDimension($cells)->setAutoSize(true);
			 		$objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($style);
			 	}
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($styleColor);
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold( true )->getColor()->setRGB('FFFFFF');


				$objPHPExcel->getActiveSheet()->setCellValue('A1','Nomor');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Id Monitor');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Tipe');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Tahun');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Ruangan');
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Kondisi');


				$baris = 2;
				$x=1;

				foreach ($data['inven'] as $item) {
					# code...
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris, $x);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris, $item->id_monitor);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris, $item->type);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris, $item->tahun);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, $item->ruangan, $item->gedung);
					if($item->status == 1){
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Layak");
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Tidak Layak");
					}
					$baris++;
					$x++;
				}
				$objPHPExcel->getActiveSheet()->getStyle("A1".":F".$x)->applyFromArray($BStyle);
				$filename = "Data Monitor".date("d-m-Y-H-i-s").'.xlsx';
				$objPHPExcel->getActiveSheet()->setTitle("Data Monitor");

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age = 0');
				$writer = PHPExcel_IOFactory::createwriter($objPHPExcel, 'Excel2007');
				$writer->save('php://output');
				exit;

				break;




				case 'PC' :

				$data['inven'] = $this->md_pc->list_all();

				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
				require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("idr corner");
				$objPHPExcel->getProperties()->setLastModifiedBy("idr corner");
				$objPHPExcel->getProperties()->setTitle("Data PC");


				$objPHPExcel->setActiveSheetIndex(0);

				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			        )

			    );
			    $styleColor = array(
			    	'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '5a6e8f')
			        )
			    );
			    $BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_THICK,
				      'color' => array('rgb' => '5a6e8f')
				    )
				  )
				);
			 	for($cells = "A"; $cells <= "F"; $cells++){
			 		$objPHPExcel->getActiveSheet()->getColumnDimension($cells)->setAutoSize(true);
			 		$objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($style);
			 	}
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($styleColor);
			 	$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold( true )->getColor()->setRGB('FFFFFF');


				$objPHPExcel->getActiveSheet()->setCellValue('A1','Nomor');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Id PC');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Tipe');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Tahun');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Ruangan');
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Kondisi');


				$baris = 2;
				$x=1;

				foreach ($data['inven'] as $item) {
					# code...
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris, $x);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris, $item->id_pc);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris, $item->type);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris, $item->tahun);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris, $item->ruangan, $item->gedung);
					if($item->status == 1){
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Layak");
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris, "Tidak Layak");
					}
					$baris++;
					$x++;
				}
				$objPHPExcel->getActiveSheet()->getStyle("A1".":F".$x)->applyFromArray($BStyle);
				$filename = "Data PC".date("d-m-Y-H-i-s").'.xlsx';
				$objPHPExcel->getActiveSheet()->setTitle("Data PC");

				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age = 0');
				$writer = PHPExcel_IOFactory::createwriter($objPHPExcel, 'Excel2007');
				$writer->save('php://output');
				exit;

				break;

		}
	}

	public function listPapan()
	{
		$data['inven'] = $this->md_papan->list_all();
		$data['gedung'] = $this->md_papan->gedung();
		$data['cor'] = $this->md_papan->tb_jenis_inventaris();
		$this->load->view('admin/dashboard/inventory/inventaris_PapanTulis',$data);
	}

	public function listPrinter()
	{
		$data['inven'] = $this->md_printer->list_all();
		$data['gedung'] = $this->md_printer->gedung();
		$data['cor'] = $this->md_printer->tb_jenis_inventaris();
		$this->load->view('admin/dashboard/inventory/inventaris_printer', $data);
	}

	public function listMonitor()
	{
		$data['inven'] = $this->md_monitor->list_all();
		$data['gedung'] = $this->md_monitor->gedung();
		$data['cor'] = $this->md_monitor->tb_jenis_inventaris();
		$this->load->view('admin/dashboard/inventory/inventaris_monitor', $data);
	}
	public function listProyektor()
	{
		$data['inven'] = $this->md_proyektor->list_all();
		$data['cor'] = $this->md_proyektor->tb_jenis_inventaris();
		$data['type'] = $this->md_proyektor->type();
		//var_dump($data);
		$this->load->view('admin/dashboard/inventory/inventaris_proyektor',$data);
	}
	public function listRak()
	{
		$data['inven'] = $this->md_rak->list_all();
		$data['gedung'] = $this->md_rak->gedung();
		$data['jenis'] = $this->md_rak->jenis();
		$data['type'] = $this->md_rak->type();
		$data['cor'] = $this->md_rak->tb_jenis_inventaris();
		$this->load->view('admin/dashboard/inventory/inventaris_rak',$data);
	}
	public function listStapol()
	{
		$data['inven'] = $this->md_stapol->list_all();
		$data['gedung'] = $this->md_stapol->gedung();
		$data['cor'] = $this->md_stapol->tb_jenis_inventaris();
		$this->load->view('admin/dashboard/inventory/inventaris_stapol', $data);
	}

	public function listMeja()
	{
		$data['inven'] = $this->md_meja->list_all();
		$data['gedung'] = $this->md_meja->gedung();
		$data['jenis'] = $this->md_meja->jenis();
		$data['type'] = $this->md_meja->type();
		$data['cor'] = $this->md_meja->tb_jenis_inventaris();
		$this->load->view('admin/dashboard/inventory/inventaris_meja',$data);
	}
	public function listPeminjamanProyektor()
	{
		$data['pinjam'] = $this->md_pinjamProyektor->list_all();
		$data['proyektor'] = $this->md_pinjamProyektor->tb_proyektor();
		$data['gedung'] = $this->md_pinjamProyektor->gedung();
		//var_dump($data);
		$this->load->view('admin/dashboard/Peminjaman/peminjaman_proyektor',$data);
	}
	public function ruangan()//jangan di rubah
	{
		// POST data
	    $postData = $this->input->post();
	    // get data
	    $data = $this->md_kursi->ruangan($postData);
	    echo json_encode($data);
	}
	public function kembalikanProyektor()
	{
		$id=$this->uri->segment('4');
		$data['status'] = $this->uri->segment('3');
		if ($this->uri->segment('3') == 0 ) {
			$data['tgl_kembali'] = null;
		}
		else
		{
			$data['tgl_kembali'] = date('Y-m-d');
		}
			
		$this->md_pinjamProyektor->kembaliProyektor($id,$data);
		redirect(base_url('Inventaris/listPeminjamanProyektor'));

	}
	public function addNew()
	{
		if( empty($this->uri->segment('3'))) {
			redirect( base_url() );
		}

		$cek=$this->uri->segment('3');
		//var_dump($cek);

		switch ($cek) {
			case 'kursi':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id'));
					$jenis_kursi= $this->security->xss_clean( $this->input->post('jenisKursi'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					if ($this->input->post('loop') == null) {
						$loop = 1;
					}
					else
					{
						$loop= $this->security->xss_clean( $this->input->post('loop'));
					}

					// validasi
					$this->form_validation->set_rules('id', 'ID Kursi', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Kursi');
						redirect( base_url('Inventaris/listKursi') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Kursi')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;
					var_dump($id_kode_barang);


					for ($j=0; $j < $loop ; $j++) {
			            $data['id_kursi'] = $id+$j;
						$data['id_jenis_kursi'] = $jenis_kursi;
						$data['id_ruangan'] = $ruangan;
						$data['status'] = $kondisi;
						$data['id_kode_barang'] = $id_kode_barang;
						$this->md_kursi->tambahKursi($data);
			        }

					redirect(base_url('Inventaris/listKursi'));
				}
				break;


				case 'jenisKursi':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {

					$nama= $this->security->xss_clean( $this->input->post('nama'));
					$ukuran= $this->security->xss_clean( $this->input->post('ukuran'));

					// validasi
					$this->form_validation->set_rules('nama', 'Nama Jenis Kursi', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert_error', 'Gagal Menambah data Jenis Kursi');
						redirect( base_url('Inventaris/listKategoriMeja') );
					}

					$upload = $this->md_jKursi->upload();
					//var_dump($upload);
					if($upload['result'] == "success")
					{
						$status = 1;
						$this->md_jKursi->save($upload);
						redirect(base_url('Inventaris/listKategoriKursi'));
					}
					else
					{
						$data['message'] = $upload['error'];
					}
				}
				break;


				case 'jenisRak':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {

					$nama= $this->security->xss_clean( $this->input->post('nama'));
					$ukuran= $this->security->xss_clean( $this->input->post('ukuran'));

					// validasi
					$this->form_validation->set_rules('nama', 'Nama Jenis Rak', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert_error', 'Gagal Menambah data Jenis Rak');
						redirect( base_url('Inventaris/listKategoriRak') );
					}

					$upload = $this->md_jRak->upload();
					//var_dump($upload);
					if($upload['result'] == "success")
					{
						$status = 1;
						$this->md_jRak->save($upload);
						redirect(base_url('Inventaris/listKategoriRak'));
					}
					else
					{
						$data['message'] = $upload['error'];
					}
				}
				break;



				case 'pc':

				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					if ($this->input->post('loop') == null) {
						$loop = 1;
					}
					else
					{
						$loop= $this->security->xss_clean( $this->input->post('loop'));
					}

					// validasi
					$this->form_validation->set_rules('id', 'ID PC', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data PC');
						redirect( base_url('Inventaris/listPC') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','PC')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;
					// var_dump($id_kode_barang);

					for ($j=0; $j < $loop ; $j++) {
						$data['id_pc'] = $id+$j;
						$data['id_ruangan'] = $ruangan;
						$data['status'] = $kondisi;
						$data['id_kode_barang'] = $id_kode_barang;
						$this->md_pc->tambahPC($data);
					}

					redirect(base_url('Inventaris/listPC'));
				}

				break;
				case 'ac':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					if ($this->input->post('loop') == null) {
						$loop = 1;
					}
					else
					{
						$loop= $this->security->xss_clean( $this->input->post('loop'));
					}

					// validasi
					$this->form_validation->set_rules('id', 'ID AC', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data AC');
						redirect( base_url('Inventaris/listAC') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','AC')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;
					// var_dump($id_kode_barang);

					for ($j=0; $j < $loop ; $j++) {
						$data['id_ac'] = $id+$j;
						$data['id_ruangan'] = $ruangan;
						$data['status'] = $kondisi;
						$data['id_kode_barang'] = $id_kode_barang;
						$this->md_ac->tambahAC($data);
					}

					redirect(base_url('Inventaris/listAC'));
				}
				break;


				case 'papan':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					if ($this->input->post('loop') == null) {
						$loop = 1;
					}
					else
					{
						$loop= $this->security->xss_clean( $this->input->post('loop'));
					}

					// validasi
					$this->form_validation->set_rules('id', 'ID Papan', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Papan');
						redirect( base_url('Inventaris/listPapan') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Papan')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;
					// var_dump($id_kode_barang);

					for ($j=0; $j < $loop ; $j++) {
						$data['id_papan'] = $id+$j;
						$data['id_ruangan'] = $ruangan;
						$data['status'] = $kondisi;
						$data['id_kode_barang'] = $id_kode_barang;
						$this->md_papan->tambahPapan($data);
					}

					redirect(base_url('Inventaris/listPapan'));
				}
				break;



				case 'rak':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id'));
					$jenis_rak= $this->security->xss_clean( $this->input->post('jenisRak'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$kode_barang= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));
					if ($this->input->post('loop') == null) {
						$loop = 1;
					}
					else
					{
						$loop= $this->security->xss_clean( $this->input->post('loop'));
					}

					// validasi
					$this->form_validation->set_rules('id', 'ID Rak', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Rak');
						redirect( base_url('Inventaris/listRak') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('kode_barang',$kode_barang)
								->where('tahun',$tahun)
								->where('nama_barang','Rak')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;


					for ($j=0; $j < $loop ; $j++) {
			            $data['id_rak'] = $id+$j;
						$data['id_jenis_rak'] = $jenis_rak;
						$data['id_ruangan'] = $ruangan;
						$data['status'] = $kondisi;
						$data['id_kode_barang'] = $id_kode_barang;
						$this->md_rak->tambahRak($data);
			        }

					redirect(base_url('Inventaris/listRak'));
				}
				break;

				case 'stapol':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					if ($this->input->post('loop') == null) {
						$loop = 1;
					}
					else
					{
						$loop= $this->security->xss_clean( $this->input->post('loop'));
					}

					// validasi
					$this->form_validation->set_rules('id', 'ID Stapol', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Stapol');
						redirect( base_url('Inventaris/listStapol') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Stapol')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;
					// var_dump($id_kode_barang);

					for ($j=0; $j < $loop ; $j++) {
						$data['id_stapol'] = $id+$j;
						$data['id_ruangan'] = $ruangan;
						$data['status'] = $kondisi;
						$data['id_kode_barang'] = $id_kode_barang;
						$this->md_stapol->tambahStapol($data);
					}

					redirect(base_url('Inventaris/listStapol'));
				}
				break;



				case 'monitor':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					if ($this->input->post('loop') == null) {
						$loop = 1;
					}
					else
					{
						$loop= $this->security->xss_clean( $this->input->post('loop'));
					}

					// validasi
					$this->form_validation->set_rules('id', 'ID Monitor', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Monitor');
						redirect( base_url('Inventaris/listMonitor') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Monitor')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;
					// var_dump($id_kode_barang);

					for ($j=0; $j < $loop ; $j++) {
						$data['id_monitor'] = $id+$j;
						$data['id_ruangan'] = $ruangan;
						$data['status'] = $kondisi;
						$data['id_kode_barang'] = $id_kode_barang;
						$this->md_monitor->tambahMonitor($data);
					}

					redirect(base_url('Inventaris/listMonitor'));
				}
				break;


				case 'printer':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					if ($this->input->post('loop') == null) {
						$loop = 1;
					}
					else
					{
						$loop= $this->security->xss_clean( $this->input->post('loop'));
					}

					// validasi
					$this->form_validation->set_rules('id', 'ID Printer', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Printer');
						redirect( base_url('Inventaris/listPrinter') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Printer')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;
					// var_dump($id_kode_barang);

					for ($j=0; $j < $loop ; $j++) {
						$data['id_printer'] = $id+$j;
						$data['id_ruangan'] = $ruangan;
						$data['status'] = $kondisi;
						$data['id_kode_barang'] = $id_kode_barang;
						$this->md_printer->tambahPrinter($data);
					}

					redirect(base_url('Inventaris/listPrinter'));
				}
				break;


				case 'meja':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id'));
					$jenis_meja= $this->security->xss_clean( $this->input->post('jenisMeja'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$kode_barang= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));
					if ($this->input->post('loop') == null) {
						$loop = 1;
					}
					else
					{
						$loop= $this->security->xss_clean( $this->input->post('loop'));
					}

					// validasi
					$this->form_validation->set_rules('id', 'ID Meja', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Meja');
						redirect( base_url('Inventaris/listMeja') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('kode_barang',$kode_barang)
								->where('tahun',$tahun)
								->where('nama_barang','Meja')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;


					for ($j=0; $j < $loop ; $j++) {
				        $data['id_meja'] = $id+$j;
						$data['id_jenis_meja'] = $jenis_meja;
						$data['id_ruangan'] = $ruangan;
						$data['status'] = $kondisi;
						$data['id_kode_barang'] = $id_kode_barang;
						$this->md_meja->tambahMeja($data);
				    }

					redirect(base_url('Inventaris/listMeja'));
				}
				break;

				case 'jenisMeja':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {

					$nama= $this->security->xss_clean( $this->input->post('nama'));
					$ukuran= $this->security->xss_clean( $this->input->post('ukuran'));

					// validasi
					$this->form_validation->set_rules('nama', 'Nama Jenis Meja', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert_error', 'Gagal Menambah data Jenis Meja');
						redirect( base_url('Inventaris/listKategoriMeja') );
					}

					$upload = $this->md_jMeja->upload();
					//var_dump($upload);
					if($upload['result'] == "success")
					{
						$status = 1;
						$this->md_jMeja->save($upload);
						redirect(base_url('Inventaris/listKategoriMeja'));
					}
					else
					{
						$data['message'] = $upload['error'];
					}
				}
				break;

				case 'proyektor':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// var_dump($id);

					if ($this->input->post('loop') == null) {
						$loop = 1;
					}
					else
					{
						$loop= $this->security->xss_clean( $this->input->post('loop'));
					}

					// validasi
					$this->form_validation->set_rules('id', 'Kode Proyektor Diperlukan', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Proyektor');
						redirect( base_url('Inventaris/listProyektor') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('kode_barang',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Proyektor')
								->get();

					$id_kode_barang = $x->row()->id_kode_barang;

					// var_dump($type);

					for ($j=0; $j < $loop ; $j++) {
						$data['id_proyektor'] = $id+$j;
						$data['status'] = $kondisi;
						$data['id_kode_barang'] = $id_kode_barang;
						$this->md_proyektor->tambahProyektor($data);
					}

					 redirect(base_url('Inventaris/listProyektor'));
				}
				break;

				case 'KatInven':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {

					$kode_barang= $this->security->xss_clean( $this->input->post('kode_barang'));
					$id_univ= $this->security->xss_clean( $this->input->post('id_univ'));
					$nama_barang= $this->security->xss_clean( $this->input->post('nama_barang'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					//var_dump($kode_barang);
					$this->form_validation->set_rules('kode_barang', 'Kode Barang', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert_error', 'Gagal Menambah data Kode Barang');
						redirect( base_url('Inventaris/KategoriInventaris') );
					}

					$upload = $this->md_katInven->upload();
					//var_dump($upload);
					if($upload['result'] == "success")
					{
						$this->md_katInven->save($upload);
						redirect(base_url('Inventaris/KategoriInventaris'));
					}
					else
					{
						$data['message'] = $upload['error'];
					}
				}
				break;

				case 'pinjamProyektor':

				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_proyektor= $this->security->xss_clean( $this->input->post('id_proyektor'));
					$tgl_ambil= $this->security->xss_clean( $this->input->post('tgl_ambil'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$nama= $this->security->xss_clean( $this->input->post('nama'));
					$nim= $this->security->xss_clean( $this->input->post('nim'));

					// var_dump($id_proyektor);
					// validasi
					$this->form_validation->set_rules('id_proyektor', 'Kode Proyektor Diperlukan', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Peminjaman Proyektor');
						redirect( base_url('Inventaris/listPeminjamanProyektor') );
					}

					$x = $this->db->select('*')
								->from('tb_peminjaman_proyektor')
								->order_by('id_peminjaman','DESC')
								->get();
					$id = $x->row()->id_peminjaman;

					$data['id_peminjaman'] = $id+1;
					$data['id_proyektor'] = $id_proyektor;
					$data['id_ruangan'] = $ruangan;
					$data['tgl_ambil'] = $tgl_ambil;
					$data['nama'] = $nama;
					$data['nim'] = $nim;
					$data['status'] = 0;
					// var_dump($data);
					$this->md_pinjamProyektor->pinjamProyektor($data);

					redirect(base_url('Inventaris/listPeminjamanProyektor'));
				}
				break;

			default:
				redirect( base_url() );
				break;
		}
	}
	public function hapus()
	{
		if( empty($this->uri->segment('3'))) {
			redirect( base_url() );
		}

		if( empty($this->uri->segment('4'))) {
			redirect( base_url() );
		}

		$cek = $this->uri->segment('3');
		$id = $this->uri->segment('4');
		//var_dump($id);

		switch ($cek) {
			case 'Kursi':
				$this->md_kursi->hapusKursi($id);
			    redirect(base_url('Inventaris/listKursi'));
				break;
			case 'jenisKursi':
				$this->md_jKursi->hapusJenisKursi($id);
			    redirect(base_url('Inventaris/listKategoriKursi'));
				break;
			case 'Meja':
				$this->md_meja->hapusMeja($id);
			    redirect(base_url('Inventaris/listMeja'));
				break;

				$this->md_kursi->haspuKursi($id);
			    redirect(base_url('Inventaris/listKursi'));
				break;
			case 'AC':
				$this->md_ac->hapusAc($id);
				redirect(base_url('Inventaris/listAc'));
			break;
			case 'jenisMeja':
				$this->md_jMeja->hapusJenisMeja($id);
			    redirect(base_url('Inventaris/listKategoriMeja'));
				break;
			case 'Rak':
				$this->md_rak->hapusRak($id);
			    redirect(base_url('Inventaris/listRak'));
			break;
			case 'jenisRak':
				$this->md_jRak->hapusJenisRak($id);
			    redirect(base_url('Inventaris/listKategoriRak'));
			break;
			case 'Monitor':
				$this->md_monitor->hapusMonitor($id);
				redirect(base_url('Inventaris/listMonitor'));
			break;
			case 'Stapol':
				$this->md_stapol->hapusStapol($id);
				redirect(base_url('Inventaris/listStapol'));
			break;
			case 'PC':
				$this->md_pc->hapusPc($id);
				redirect(base_url('Inventaris/listPC'));
			break;
			case 'Papan':
				$this->md_papan->hapusPapan($id);
				redirect(base_url('Inventaris/listPapan'));
			break;
			case 'Printer':
				$this->md_printer->hapusPapan($id);
				redirect(base_url('Inventaris/listPrinter'));
			break;
			case 'Proyektor':
				$this->md_proyektor->hapusProyektor($id);
				redirect(base_url('Inventaris/listProyektor'));
			break;
			case 'KatInven':
				$this->md_katInven->hapusKatInven($id);
			    redirect(base_url('Inventaris/KategoriInventaris'));
				break;
			case 'peminjamanProyektor':
				$this->md_pinjamProyektor->hapusPeminjamanProyektor($id);
			    redirect(base_url('Inventaris/listPeminjamanProyektor'));
				break;
			default:
				redirect( base_url() );
				break;
		}
	}

	public function edit()
	{
		if( empty($this->uri->segment('3'))) {
			redirect( base_url() );
		}

		if( empty($this->uri->segment('4'))) {
			redirect( base_url() );
		}
		$cek = $this->uri->segment('3');
		$id = $this->uri->segment('4');

		switch ($cek) {
			case 'Kursi':
				$data['item']=$this->md_kursi->editKursi($id);
				$y = $data['item']->jenisKursi;
				$data['gedung'] = $this->md_kursi->gedung();
				$data['jenis'] = $this->md_kursi->jenis();
				$data['x'] = $this->md_kursi->tb_jenis_inventaris();
				
			    $this->load->view('admin/dashboard/inventory/inventarisKursiEdit',$data);
				break;
			case 'Monitor':
				$data['item']=$this->md_monitor->editMonitor($id);
				$data['gedung'] = $this->md_monitor->gedung();
				$data['x'] = $this->md_monitor->tb_jenis_inventaris();
				//var_dump($data);
			    $this->load->view('admin/dashboard/inventory/inventarisMonitorEdit',$data);
				break;
			case 'Rak':
				$data['item']=$this->md_rak->editRak($id);
				$data['gedung'] = $this->md_rak->gedung();
				$data['jenis'] = $this->md_rak->jenis();
				$data['x'] = $this->md_rak->tb_jenis_inventaris();
				//var_dump($data);
			    $this->load->view('admin/dashboard/inventory/inventarisRakEdit',$data);
			break;
			case 'Stapol':
				$data['item']=$this->md_stapol->editStapol($id);	
				$data['gedung'] = $this->md_stapol->gedung();
				$data['x'] = $this->md_stapol->tb_jenis_inventaris();
				//var_dump($data);
			    $this->load->view('admin/dashboard/inventory/inventarisStapolEdit',$data);
			break;
			case 'Meja':
				$data['item']=$this->md_meja->editMeja($id);
				$data['gedung'] = $this->md_meja->gedung();
				$data['jenis'] = $this->md_meja->jenis();
				$data['x'] = $this->md_meja->tb_jenis_inventaris();
				//var_dump($data);
			    $this->load->view('admin/dashboard/inventory/inventarisMejaEdit',$data);
			break;
			case 'PC':
				$data['item']=$this->md_pc->editPC($id);
				$data['gedung'] = $this->md_pc->gedung();
				$data['x'] = $this->md_pc->tb_jenis_inventaris();
				//var_dump($data);
			    $this->load->view('admin/dashboard/inventory/inventarisPcEdit',$data);
				break;
			case 'AC':
				$data['item']=$this->md_ac->editAC($id);
				$data['gedung'] = $this->md_ac->gedung();
				$data['x'] = $this->md_ac->tb_jenis_inventaris();
				//var_dump($data);
			    $this->load->view('admin/dashboard/inventory/inventarisAcEdit',$data);
				break;
			case 'Papan':
				$data['item']=$this->md_papan->editPapan($id);
				$data['gedung'] = $this->md_papan->gedung();
				$data['x'] = $this->md_papan->tb_jenis_inventaris();
				//var_dump($data);
			    $this->load->view('admin/dashboard/inventory/inventarisPapanTulisEdit',$data);
				break;
			case 'Printer':
				$data['item']=$this->md_printer->editPrinter($id);
				$data['gedung'] = $this->md_printer->gedung();
				$data['x'] = $this->md_printer->tb_jenis_inventaris();
				//var_dump($data);
			    $this->load->view('admin/dashboard/inventory/inventarisPrinterEdit',$data);
				break;
			case 'Proyektor':
				$data['item']=$this->md_proyektor->editProyektor($id);
				$data['x'] = $this->md_proyektor->tb_jenis_inventaris();
				//var_dump($data);
			    $this->load->view('admin/dashboard/inventory/inventarisProyektorEdit',$data);
				break;
			default:
				redirect( base_url() );
				break;
		}
	}














	public function update()
	{
		if( empty($this->uri->segment('3'))) {
			redirect( base_url() );
		}
		$cek = $this->uri->segment('3');
		//var_dump($cek);

		switch ($cek) {
			case 'kursi':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_kursi= $this->security->xss_clean( $this->input->post('id'));
					$id_jenis_kursi= $this->security->xss_clean( $this->input->post('id_jenis_kursi'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					$this->form_validation->set_rules('id', 'ID Kursi', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Kursi');
						redirect( base_url('Inventaris/listKursi') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Kursi')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;

					$id = $id_kursi;
					$data['id_jenis_kursi'] = $id_jenis_kursi;
					$data['id_ruangan'] = $ruangan;
					$data['status'] = $kondisi;
					$data['id_kode_barang'] = $id_kode_barang;

					// var_dump($data);
					$this->md_kursi->updateKursi($id,$data);
					redirect(base_url('Inventaris/listKursi'));
				}
			break;


			case 'jenisKursi':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {

					$nama= $this->security->xss_clean( $this->input->post('nama'));
					$old= $this->security->xss_clean( $this->input->post('old'));
					$id= $this->security->xss_clean( $this->input->post('id'));

					//var_dump($id);
					//validasi
					$this->form_validation->set_rules('nama', 'Nama Jenis Kursi', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert_error', 'Gagal Edit data Jenis Kursi');
						redirect( base_url('Inventaris/listKategoriKursi') );
					}

					if (!empty($_FILES["file_gambar"]["name"])) {
						$this->md_jKursi->hapusGambar($old);
					    $upload = $this->md_jKursi->upload();

					    if($upload['result'] == "success")
						{
							$status = 1;
							$this->md_jKursi->update($id,$status,$upload);
							redirect(base_url('Inventaris/listKategoriKursi'));
						}
						else
						{
							$data['message'] = $upload['error'];
						}
					} else {
					    $status = 0;
					    $upload = 1;
						$this->md_jKursi->update($id,$status,$upload);
						redirect(base_url('Inventaris/listKategoriKursi'));
					}
				}
			break;


			case 'rak':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_rak= $this->security->xss_clean( $this->input->post('id'));
					$id_jenis_rak= $this->security->xss_clean( $this->input->post('id_jenis_rak'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					$this->form_validation->set_rules('id', 'ID Rak', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Rak');
						redirect( base_url('Inventaris/listRak') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Rak')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;

					$id = $id_rak;
					$data['id_jenis_rak'] = $id_jenis_rak;
					$data['id_ruangan'] = $ruangan;
					$data['status'] = $kondisi;
					$data['id_kode_barang'] = $id_kode_barang;

					// var_dump($data);
					$this->md_rak->updateRak($id,$data);
					redirect(base_url('Inventaris/listRak'));
				}
			break;

			case 'jenisRak':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {

					$nama= $this->security->xss_clean( $this->input->post('nama'));
					$old= $this->security->xss_clean( $this->input->post('old'));
					$id= $this->security->xss_clean( $this->input->post('id'));

					//var_dump($id);
					//validasi
					$this->form_validation->set_rules('nama', 'Nama Jenis Rak', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert_error', 'Gagal Edit data Jenis Rak');
						redirect( base_url('Inventaris/listKategoriRak') );
					}

					if (!empty($_FILES["file_gambar"]["name"])) {
						$this->md_jRak->hapusGambar($old);
					    $upload = $this->md_jRak->upload();

					    if($upload['result'] == "success")
						{
							$status = 1;
							$this->md_jRak->update($id,$status,$upload);
							redirect(base_url('Inventaris/listKategoriRak'));
						}
						else
						{
							$data['message'] = $upload['error'];
						}
					} else {
					    $status = 0;
					    $upload = 1;
						$this->md_jRak->update($id,$status,$upload);
						redirect(base_url('Inventaris/listKategoriRak'));
					}
				}
			break;

			case 'stapol':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_stapol= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					$this->form_validation->set_rules('id', 'ID Stapol', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Stapol');
						redirect( base_url('Inventaris/listStapol') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Stapol')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;

					$id = $id_stapol;
					$data['id_ruangan'] = $ruangan;
					$data['status'] = $kondisi;
					$data['id_kode_barang'] = $id_kode_barang;

					// var_dump($data);
					$this->md_stapol->updateStapol($id,$data);
					redirect(base_url('Inventaris/listStapol'));
				}
			break;

			case 'pc':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_pc= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					$this->form_validation->set_rules('id', 'ID PC', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data PC');
						redirect( base_url('Inventaris/listPC') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','PC')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;

					$id = $id_pc;
					$data['id_ruangan'] = $ruangan;
					$data['status'] = $kondisi;
					$data['id_kode_barang'] = $id_kode_barang;

					// var_dump($data);
					$this->md_pc->updatePC($id,$data);
					redirect(base_url('Inventaris/listPC'));
				}
			break;
			case 'ac':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_ac= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					$this->form_validation->set_rules('id', 'ID AC', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data AC');
						redirect( base_url('Inventaris/listPC') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','AC')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;

					$id = $id_ac;
					$data['id_ruangan'] = $ruangan;
					$data['status'] = $kondisi;
					$data['id_kode_barang'] = $id_kode_barang;

					// var_dump($data);
					$this->md_ac->updateAC($id,$data);
					redirect(base_url('Inventaris/listAC'));
				}
			break;


			case 'papan':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_papan= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					$this->form_validation->set_rules('id', 'ID Papan', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Papan');
						redirect( base_url('Inventaris/listPapan') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Papan')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;

					$id = $id_papan;
					$data['id_ruangan'] = $ruangan;
					$data['status'] = $kondisi;
					$data['id_kode_barang'] = $id_kode_barang;

					// var_dump($data);
					$this->md_papan->updatePapan($id,$data);
					redirect(base_url('Inventaris/listPapan'));
				}
			break;


			case 'printer':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_printer= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					$this->form_validation->set_rules('id', 'ID Printer', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Printer');
						redirect( base_url('Inventaris/listPrinter') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Printer')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;

					$id = $id_printer;
					$data['id_ruangan'] = $ruangan;
					$data['status'] = $kondisi;
					$data['id_kode_barang'] = $id_kode_barang;

					// var_dump($data);
					$this->md_printer->updatePrinter($id,$data);
					redirect(base_url('Inventaris/listPrinter'));
				}
			break;


			case 'monitor':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_monitor= $this->security->xss_clean( $this->input->post('id'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					$this->form_validation->set_rules('id', 'ID Monitor', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Monitor');
						redirect( base_url('Inventaris/listMonitor') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Monitor')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;

					$id = $id_monitor;
					$data['id_ruangan'] = $ruangan;
					$data['status'] = $kondisi;
					$data['id_kode_barang'] = $id_kode_barang;

					// var_dump($data);
					$this->md_monitor->updateMonitor($id,$data);
					redirect(base_url('Inventaris/listMonitor'));
				}
			break;


			case 'meja':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_meja= $this->security->xss_clean( $this->input->post('id'));
					$id_jenis_meja= $this->security->xss_clean( $this->input->post('id_jenis_meja'));
					$ruangan= $this->security->xss_clean( $this->input->post('ruangan'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					$this->form_validation->set_rules('id', 'ID Meja', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Meja');
						redirect( base_url('Inventaris/listMeja') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Meja')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;

					$id = $id_meja;
					$data['id_jenis_meja'] = $id_jenis_meja;
					$data['id_ruangan'] = $ruangan;
					$data['status'] = $kondisi;
					$data['id_kode_barang'] = $id_kode_barang;

					// var_dump($data);
					$this->md_meja->updateMeja($id,$data);
					redirect(base_url('Inventaris/listMeja'));
				}
			break;

			case 'jenisMeja':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {

					$nama= $this->security->xss_clean( $this->input->post('nama'));
					$ukuran= $this->security->xss_clean( $this->input->post('ukuran'));
					$old= $this->security->xss_clean( $this->input->post('old'));
					$id= $this->security->xss_clean( $this->input->post('id'));

					//var_dump($id);
					//validasi
					$this->form_validation->set_rules('nama', 'Nama Jenis Meja', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert_error', 'Gagal Edit data Jenis Meja');
						redirect( base_url('Inventaris/listKategoriMeja') );
					}

					if (!empty($_FILES["file_gambar"]["name"])) {
						$this->md_jMeja->hapusGambar($old);
					    $upload = $this->md_jMeja->upload();

					    if($upload['result'] == "success")
						{
							$status = 1;
							$this->md_jMeja->update($id,$status,$upload);
							redirect(base_url('Inventaris/listKategoriMeja'));
						}
						else
						{
							$data['message'] = $upload['error'];
						}
					} else {
					    $status = 0;
					    $upload = 1;
						$this->md_jMeja->update($id,$status,$upload);
						redirect(base_url('Inventaris/listKategoriMeja'));
					}
				}
			break;
			case 'Proyektor':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_proyektor= $this->security->xss_clean( $this->input->post('id'));
					$kondisi= $this->security->xss_clean( $this->input->post('kondisi'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));

					// validasi
					$this->form_validation->set_rules('id', 'ID Proyektor', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Menambah data Proyektor');
						redirect( base_url('Inventaris/listProyektor') );
					}

					$x = $this->db->select('*')
								->from('tb_jenis_inventaris')
								->where('type',$type)
								->where('tahun',$tahun)
								->where('nama_barang','Proyektor')
								->get();
					$id_kode_barang = $x->row()->id_kode_barang;

					$id = $id_proyektor;
					$data['status'] = $kondisi;
					$data['id_kode_barang'] = $id_kode_barang;

					//var_dump($data);
					$this->md_proyektor->updateProyektor($id,$data);
					redirect(base_url('Inventaris/listProyektor'));
				}
			break;
			case 'KatInven':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {

					$kode_barang= $this->security->xss_clean( $this->input->post('kode_barang'));
					$id_univ= $this->security->xss_clean( $this->input->post('id_univ'));
					$nama_barang= $this->security->xss_clean( $this->input->post('nama_barang'));
					$type= $this->security->xss_clean( $this->input->post('type'));
					$tahun= $this->security->xss_clean( $this->input->post('tahun'));
					$old= $this->security->xss_clean( $this->input->post('old'));
					$id= $this->security->xss_clean( $this->input->post('id_kode_barang'));

					//var_dump($id);
					//validasi
					$this->form_validation->set_rules('kode_barang', 'Kode Barang Dibutuhkan', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert_error', 'Gagal Edit data Jenis Inventasri');
						redirect( base_url('Inventaris/KategoriInventaris') );
					}

					if (!empty($_FILES["file_gambar"]["name"])) {
						$this->md_katInven->hapusGambar($old);
					    $upload = $this->md_katInven->upload();

					    if($upload['result'] == "success")
						{
							$status = 1;
							$this->md_katInven->update($id,$status,$upload);
							redirect(base_url('Inventaris/KategoriInventaris'));
						}
						else
						{
							$data['message'] = $upload['error'];
						}
					} else {
					    $status = 0;
					    $upload = 1;
					    var_dump($upload);
						$this->md_katInven->update($id,$status,$upload);
						redirect(base_url('Inventaris/KategoriInventaris'));
					}
				}
			break;
			case 'pinjamProyektor':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_peminjaman= $this->security->xss_clean( $this->input->post('id_peminjaman'));
					$id_proyektor= $this->security->xss_clean( $this->input->post('id_proyektor'));
					$tgl_ambil= $this->security->xss_clean( $this->input->post('tgl_ambil'));
					$ruangan= $this->security->xss_clean( $this->input->post('id_ruangan'));
					$nama= $this->security->xss_clean( $this->input->post('nama'));
					$nim= $this->security->xss_clean( $this->input->post('nim'));

					// var_dump($id_proyektor);
					// validasi
					$this->form_validation->set_rules('id_peminjaman', 'Kode Proyektor Diperlukan', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', 'Gagal Edit data Peminjaman Proyektor');
						redirect( base_url('Inventaris/listPeminjamanProyektor') );
					}

					$id = $id_peminjaman;
					$data['id_proyektor'] = $id_proyektor;
					if ($ruangan != 0) {

						$data['id_ruangan'] = $ruangan;
					}
					$data['tgl_ambil'] = $tgl_ambil;
					$data['nama'] = $nama;
					$data['nim'] = $nim;

					//var_dump($data);
					$this->md_pinjamProyektor->editPeminjamanProyektor($id,$data);
					redirect(base_url('Inventaris/listPeminjamanProyektor'));
				}
			break;
			default:
				redirect( base_url() );
				break;
		}
	}
}
