-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2019 at 10:15 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventaris`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_ac`
--

CREATE TABLE `tb_ac` (
  `id_ac` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `status` tinyint(11) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ac`
--

INSERT INTO `tb_ac` (`id_ac`, `id_ruangan`, `status`, `id_kode_barang`) VALUES
(1, 1, 1, 4),
(2, 2, 1, 4),
(7, 1, 1, 4),
(8, 1, 1, 4),
(9, 1, 1, 4),
(10, 1, 1, 4),
(11, 1, 1, 4),
(20, 2, 0, 4),
(688, 1, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tb_arsip`
--

CREATE TABLE `tb_arsip` (
  `id_arisp` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_gedung`
--

CREATE TABLE `tb_gedung` (
  `id_gedung` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_gedung`
--

INSERT INTO `tb_gedung` (`id_gedung`, `nama`) VALUES
(1, 'Gedung BC'),
(2, 'Gedung BD');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_inventaris`
--

CREATE TABLE `tb_jenis_inventaris` (
  `id_kode_barang` int(11) NOT NULL,
  `kode_barang` varchar(60) NOT NULL,
  `id_univ` varchar(40) NOT NULL,
  `nama_barang` varchar(225) NOT NULL,
  `type` varchar(225) NOT NULL,
  `tahun` year(4) NOT NULL,
  `foto` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jenis_inventaris`
--

INSERT INTO `tb_jenis_inventaris` (`id_kode_barang`, `kode_barang`, `id_univ`, `nama_barang`, `type`, `tahun`, `foto`) VALUES
(1, '3.07.01.01.127', '042.01.2200.400969.000.KD', 'Kursi', 'Tiger', 2014, NULL),
(2, '172.16.161.20', '042.01.2200.400969.000.KD', 'PC', 'simbadda', 2011, NULL),
(3, '2.12.01.02.001.1550', '042.01.2200.400969.000.KD', 'PC', 'LG', 2008, NULL),
(4, '3.05.02.04.004.65', '042.01.2200.400969.000.KD', 'AC', 'Panasonic', 2015, NULL),
(5, 'papan_0.1', '042.01.2200.400969.000.KD', 'Papan', 'V-teck', 2010, NULL),
(6, 'printer_0.1', '042.01.2200.400969.000.KD', 'Printer', 'Epson L110', 2011, NULL),
(7, '023.04.22.415253.000.2011', '042.01.2200.400969.000.KD', 'Meja', 'Uno', 2011, NULL),
(8, '3.04.01.04.004.164', '042.01.2200.400969.000.KD', 'Rak', 'ngasal', 2014, NULL),
(9, '023.04.22.415253.000.2010', '042.01.2200.400969.000.KD', 'Stapol', 'Matsugawa', 2010, NULL),
(10, 'monitor_0.1', '042.01.2200.400969.000.KD', 'Monitor', 'Samsung', 2008, NULL),
(11, '2.12.01.02.001', '042.01.2200.400969.000.KD', 'Proyektor', 'NEC', 2016, '7148bu_eka.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_kursi`
--

CREATE TABLE `tb_jenis_kursi` (
  `id_jenis_kursi` int(11) NOT NULL,
  `bahan_kursi` varchar(225) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `foto_kursi` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jenis_kursi`
--

INSERT INTO `tb_jenis_kursi` (`id_jenis_kursi`, `bahan_kursi`, `nama`, `foto_kursi`) VALUES
(1, 'Plastik', 'Kursi Plastik', '9015inbis.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_meja`
--

CREATE TABLE `tb_jenis_meja` (
  `id_jenis_meja` int(11) NOT NULL,
  `ukuran` varchar(50) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `foto_meja` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jenis_meja`
--

INSERT INTO `tb_jenis_meja` (`id_jenis_meja`, `ukuran`, `nama`, `foto_meja`) VALUES
(5, '200', 'fira', '7879haloo.PNG'),
(6, '10', 'uu', '2632haloo.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_rak`
--

CREATE TABLE `tb_jenis_rak` (
  `id_jenis_rak` int(11) NOT NULL,
  `ukuran` varchar(50) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `foto_rak` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jenis_rak`
--

INSERT INTO `tb_jenis_rak` (`id_jenis_rak`, `ukuran`, `nama`, `foto_rak`) VALUES
(1, '100 x 100 cm', 'Rak buku', 'buku.jpg'),
(2, '100 x 100 cm', 'Rak Tv', 'tv.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kursi`
--

CREATE TABLE `tb_kursi` (
  `id_kursi` int(11) NOT NULL,
  `id_jenis_kursi` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kursi`
--

INSERT INTO `tb_kursi` (`id_kursi`, `id_jenis_kursi`, `id_ruangan`, `status`, `id_kode_barang`) VALUES
(90, 1, 1, 1, 1),
(91, 1, 1, 1, 1),
(92, 1, 1, 1, 1),
(201, 1, 1, 1, 1),
(202, 1, 1, 1, 2),
(203, 1, 1, 1, 0),
(204, 1, 1, 1, 0),
(207, 1, 1, 1, 0),
(208, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_meja`
--

CREATE TABLE `tb_meja` (
  `id_meja` int(11) NOT NULL,
  `id_jenis_meja` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_meja`
--

INSERT INTO `tb_meja` (`id_meja`, `id_jenis_meja`, `id_ruangan`, `status`, `id_kode_barang`) VALUES
(1, 1, 2, 1, 0),
(3, 1, 2, 1, 0),
(4, 1, 1, 1, 0),
(8, 1, 2, 1, 0),
(9, 1, 2, 1, 0),
(10, 1, 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_monitor`
--

CREATE TABLE `tb_monitor` (
  `id_monitor` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_monitor`
--

INSERT INTO `tb_monitor` (`id_monitor`, `id_ruangan`, `status`, `id_kode_barang`) VALUES
(1, 1, 1, 10),
(2, 1, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tb_papan`
--

CREATE TABLE `tb_papan` (
  `id_papan` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_papan`
--

INSERT INTO `tb_papan` (`id_papan`, `id_ruangan`, `status`, `id_kode_barang`) VALUES
(1, 2, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pc`
--

CREATE TABLE `tb_pc` (
  `id_pc` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pc`
--

INSERT INTO `tb_pc` (`id_pc`, `id_ruangan`, `status`, `id_kode_barang`) VALUES
(0, 1, 1, 2),
(2, 1, 1, 0),
(3, 1, 0, 2),
(89, 1, 1, 0),
(90, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_peminjaman_proyektor`
--

CREATE TABLE `tb_peminjaman_proyektor` (
  `id_peminjaman` int(11) NOT NULL,
  `id_proyektor` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `tgl_ambil` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nim` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_peminjaman_proyektor`
--

INSERT INTO `tb_peminjaman_proyektor` (`id_peminjaman`, `id_proyektor`, `id_ruangan`, `tgl_ambil`, `tgl_kembali`, `nama`, `nim`, `status`) VALUES
(1, 1, 1, '2019-10-17', '2019-10-20', 'Wahyu', 1608561043, 1),
(2, 2, 1, '2019-10-17', '2019-10-20', 'wahyu', 1608561043, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_printer`
--

CREATE TABLE `tb_printer` (
  `id_printer` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_printer`
--

INSERT INTO `tb_printer` (`id_printer`, `id_ruangan`, `status`, `id_kode_barang`) VALUES
(1, 1, 1, 6),
(2, 1, 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tb_proyektor`
--

CREATE TABLE `tb_proyektor` (
  `id_proyektor` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_proyektor`
--

INSERT INTO `tb_proyektor` (`id_proyektor`, `status`, `id_kode_barang`) VALUES
(1, 1, 11),
(2, 0, 11),
(3, 1, 11),
(4, 1, 11);

-- --------------------------------------------------------

--
-- Table structure for table `tb_rak`
--

CREATE TABLE `tb_rak` (
  `id_rak` int(11) NOT NULL,
  `id_jenis_rak` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_rak`
--

INSERT INTO `tb_rak` (`id_rak`, `id_jenis_rak`, `id_ruangan`, `status`, `id_kode_barang`) VALUES
(5, 2, 1, 0, 8),
(6, 2, 1, 0, 8),
(7, 2, 1, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tb_ruangan`
--

CREATE TABLE `tb_ruangan` (
  `id_ruangan` int(11) NOT NULL,
  `id_gedung` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ruangan`
--

INSERT INTO `tb_ruangan` (`id_ruangan`, `id_gedung`, `nama`) VALUES
(1, 1, 'Ruangan BC.11'),
(2, 1, 'Ruangan BC.12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_stapol`
--

CREATE TABLE `tb_stapol` (
  `id_stapol` int(11) NOT NULL,
  `id_ruangan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_kode_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_stapol`
--

INSERT INTO `tb_stapol` (`id_stapol`, `id_ruangan`, `status`, `id_kode_barang`) VALUES
(4, 1, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jabatan` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `jabatan`, `created_at`, `updated_at`) VALUES
(1608561043, 'wahyu', 'wahyu@gmail.com', '$2y$10$lZcx14U38ny1n4THsoWIne3BNPAyz7OBNczkaxYFn52evPiEyc3iG', 'keZcyceHtUJuDZJ7HJOhtfTZX8oiDSOgvHGqLUexRxfjClSlBvucxVrJ2DfK', 1, '2019-03-26 06:14:27', '2019-03-26 06:14:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_ac`
--
ALTER TABLE `tb_ac`
  ADD PRIMARY KEY (`id_ac`);

--
-- Indexes for table `tb_arsip`
--
ALTER TABLE `tb_arsip`
  ADD PRIMARY KEY (`id_arisp`);

--
-- Indexes for table `tb_gedung`
--
ALTER TABLE `tb_gedung`
  ADD PRIMARY KEY (`id_gedung`);

--
-- Indexes for table `tb_jenis_inventaris`
--
ALTER TABLE `tb_jenis_inventaris`
  ADD PRIMARY KEY (`id_kode_barang`);

--
-- Indexes for table `tb_jenis_kursi`
--
ALTER TABLE `tb_jenis_kursi`
  ADD PRIMARY KEY (`id_jenis_kursi`);

--
-- Indexes for table `tb_jenis_meja`
--
ALTER TABLE `tb_jenis_meja`
  ADD PRIMARY KEY (`id_jenis_meja`);

--
-- Indexes for table `tb_jenis_rak`
--
ALTER TABLE `tb_jenis_rak`
  ADD PRIMARY KEY (`id_jenis_rak`);

--
-- Indexes for table `tb_kursi`
--
ALTER TABLE `tb_kursi`
  ADD PRIMARY KEY (`id_kursi`);

--
-- Indexes for table `tb_meja`
--
ALTER TABLE `tb_meja`
  ADD PRIMARY KEY (`id_meja`);

--
-- Indexes for table `tb_monitor`
--
ALTER TABLE `tb_monitor`
  ADD PRIMARY KEY (`id_monitor`);

--
-- Indexes for table `tb_papan`
--
ALTER TABLE `tb_papan`
  ADD PRIMARY KEY (`id_papan`);

--
-- Indexes for table `tb_pc`
--
ALTER TABLE `tb_pc`
  ADD PRIMARY KEY (`id_pc`);

--
-- Indexes for table `tb_peminjaman_proyektor`
--
ALTER TABLE `tb_peminjaman_proyektor`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `tb_printer`
--
ALTER TABLE `tb_printer`
  ADD PRIMARY KEY (`id_printer`);

--
-- Indexes for table `tb_proyektor`
--
ALTER TABLE `tb_proyektor`
  ADD PRIMARY KEY (`id_proyektor`);

--
-- Indexes for table `tb_rak`
--
ALTER TABLE `tb_rak`
  ADD PRIMARY KEY (`id_rak`);

--
-- Indexes for table `tb_ruangan`
--
ALTER TABLE `tb_ruangan`
  ADD PRIMARY KEY (`id_ruangan`);

--
-- Indexes for table `tb_stapol`
--
ALTER TABLE `tb_stapol`
  ADD PRIMARY KEY (`id_stapol`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_gedung`
--
ALTER TABLE `tb_gedung`
  MODIFY `id_gedung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_jenis_inventaris`
--
ALTER TABLE `tb_jenis_inventaris`
  MODIFY `id_kode_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_jenis_kursi`
--
ALTER TABLE `tb_jenis_kursi`
  MODIFY `id_jenis_kursi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_jenis_meja`
--
ALTER TABLE `tb_jenis_meja`
  MODIFY `id_jenis_meja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_jenis_rak`
--
ALTER TABLE `tb_jenis_rak`
  MODIFY `id_jenis_rak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_peminjaman_proyektor`
--
ALTER TABLE `tb_peminjaman_proyektor`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_ruangan`
--
ALTER TABLE `tb_ruangan`
  MODIFY `id_ruangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
